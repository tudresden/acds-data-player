/*
 * Created at 16:23:21 on 03.12.2014
 */
package de.tudresden.inf.rn.zeebus;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import de.tudresden.inf.rn.zeebus.emulation.client.AbstractEmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.ClientEmulator;
import de.tudresden.inf.rn.zeebus.emulation.client.Logging;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.emulation.events.SensorEvent;
import de.tudresden.inf.rn.zeebus.emulation.server.ServerEmulator;
import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
final class MockEmulatorFactory extends AbstractEmulatorFactory {
    /**
     * @param testName
     */
    MockEmulatorFactory(String testName) {
        super(testName);
    }

    @Override
    public Collection<UserEmulator> createUsers(final Logging logging) {
        return Collections.emptySet();
    }

    @Override
    public Collection<ServerEmulator> createServers(final Logging logging) {
        return Collections.emptySet();
    }

    @Override
    public Collection<? extends ClientEmulator<?>> createClients(final Logging logging) {
        Collection<ClientEmulator<?>> ret = new HashSet<>();
        EventQueue<SensorEvent<String>> eventQueue = new EventQueue<>();
        for (long i = 0l; i < 20l; i++) {
            List<SensorEvent<String>> events = new ArrayList<>();
            final String index = String.valueOf(i);
            events.add(new SensorEvent<String>() {

                @Override
                public String getMessage() {
                    return index;
                }

                @Override
                protected DateFormat getDateformat() {
                    return null;
                }
            });
            eventQueue.put(i * 1000l, events);
        }
        ClientEmulator<SensorEvent<String>> emu = new MockClientEmulator("test1", "test", eventQueue);
        ret.add(emu);
        ret.add(new MockClientEmulator("test2", "test", new EventQueue<>(eventQueue)));
        return ret;
    }
}