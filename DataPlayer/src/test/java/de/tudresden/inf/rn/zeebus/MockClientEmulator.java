/*
 * Created at 16:22:45 on 03.12.2014
 */
package de.tudresden.inf.rn.zeebus;

import java.util.List;
import java.util.Map.Entry;

import de.tudresden.inf.rn.zeebus.emulation.client.AbstractClientEmulator;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.emulation.events.SensorEvent;

class MockClientEmulator extends AbstractClientEmulator<SensorEvent<String>> {

    private final EventQueue<SensorEvent<String>> eventQueue;

    protected MockClientEmulator(final String identifier, final String testName,
                                 final EventQueue<SensorEvent<String>> eventQueue) {
        super(identifier, testName, eventQueue);
        this.eventQueue = eventQueue;
    }

    @Override
    public boolean isQueueEmpty() {
        return eventQueue.isEmpty();
    }

    @Override
    public EventQueue<SensorEvent<String>> getEventQueue() {
        return new EventQueue<>(eventQueue);
    }

    @Override
    public void connect() {
        System.out.println("connecting " + getIdentifier());
    }

    @Override
    protected List<SensorEvent<String>> popCurrentEvents() {
        Long currentTimeStamp = eventQueue.firstKey();
        return eventQueue.remove(currentTimeStamp);
    }

    @Override
    protected void sendMessage(SensorEvent<String> event) {
        System.out.println(getIdentifier() + " sending " + event.getMessage());
    }

    @Override
    protected List<SensorEvent<String>> removeEvents(long timestamp) {
        return eventQueue.remove(timestamp);
    }

    @Override
    protected Entry<Long, List<SensorEvent<String>>> getEarliestEvents() {
        return eventQueue.firstEntry();
    }

}