/*
 * Created at 14:32:06 on 24.07.2014
 */
package de.tudresden.inf.rn.zeebus;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Calendar;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;

import de.tudresden.inf.rn.zeebus.simulation.SimulationTimer;
import de.tudresden.inf.rn.zeebus.simulation.TimerListener;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public class SimulationTimerTest {

    /**
     * @author Klemens Muthmann
     *
     * @version 1.0
     * @since 1.0
     */
    private final class TimerObserver implements TimerListener {
        private int tickCounter = 0;
        private Condition condition;
        private Lock lock;

        public TimerObserver(final Lock lock, final Condition condition) {
            this.condition = condition;
            this.lock = lock;
        }

        @Override
        public void tick(final long tickTime) {
            tickCounter++;
        }

        public Integer getTickCounter() {
            return tickCounter;
        }

        @Override
        public void timerFinished() {
            lock.lock();
            try {
                condition.signal();
            } finally {
                lock.unlock();
            }
        }

        @Override
        public void timerStarted(SimulationTimer timer) {
        }
    }

    @Test
    public void testNormal() throws InterruptedException {
        Calendar simulatedStartingDate = Calendar.getInstance(); // test simuliert den zeitraum ab diesem Zeitpunkt
        simulatedStartingDate.set(2014, Calendar.JULY, 23, 10, 11);
        Calendar simulationStartingTime = Calendar.getInstance(); // mein test soll heute nach laufen
        Calendar endTime = Calendar.getInstance(); // test simuliert den zeitraum bis zu diesem Zeitpunkt
        endTime.set(2014, Calendar.JULY, 23, 10, 13);
        long simRefreshRate = 2; // alle zwei millisekunden ein tick
        long timeResolution = 1000; // pro tick um eine sekunde weiter

        SimulationTimer oocut = new SimulationTimer(simulatedStartingDate.getTimeInMillis(), endTime.getTimeInMillis(),
                0L, simulationStartingTime.getTimeInMillis(), simRefreshRate, timeResolution);

        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        TimerObserver observer = new TimerObserver(lock, condition);

        oocut.addTimerListener(observer);

        oocut.start(3l);

        lock.lock();
        try {
            condition.await();
        } finally {
            lock.unlock();
        }

        assertThat(observer.getTickCounter(), is(121));
    }
}
