/*
 * Created at 15:33:11 on 30.07.2014
 */
package de.tudresden.inf.rn.zeebus;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;

import de.tudresden.inf.rn.zeebus.emulation.EmulationController;
import de.tudresden.inf.rn.zeebus.emulation.EmulationControllerListener;
import de.tudresden.inf.rn.zeebus.emulation.client.EmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.Logging;
import de.tudresden.inf.rn.zeebus.simulation.SimulationTimer;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public class DataPlayerTest {

    @Test
    public void testStandardRun() throws InterruptedException {
        EmulatorFactory factory = new MockEmulatorFactory("test");

        SimulationTimer simulationTimer = new SimulationTimer(0l, 20000l, 5l, new Date().getTime(), 2, 1000l);
        // simulationTimer.addTimerListener(MainWindow.getMainWindow());

        final ReentrantLock lock = new ReentrantLock();
        final Condition condition = lock.newCondition();

        // Setup of emulation sender clients
        final EmulationController emulationController = new EmulationController(4, Logging.INACTIVE);
        emulationController.deploy(factory);
        simulationTimer.addTimerListener(emulationController);
        emulationController.setEmulationControllerListener(new EmulationControllerListener() {

            @Override
            public void finishedEmulation() {
                lock.lock();
                try {
                    condition.signal();
                } finally {
                    lock.unlock();
                }
            }
        });
        simulationTimer.start(4l);

        // Wait for asynchronous test to be finished.
        lock.lock();
        try {
            condition.await();
        } finally {
            lock.unlock();
        }
        // assertThat(emu.sentMessages, is(84));
    }

    @Test
    public void testDataPlayerInitialization() throws Exception {
        String properties = "simulatedStartTime=2005-08-26T00:00:00-06:00\n"
                + "simulatedEndingTime=2005-08-27T00:00:00-06:00\n" + "minDelay=5\n" + "simRefreshRate=100\n"
                + "timeResolution=1000";

        DataPlayer dp = new DataPlayer() {

            @Override
            protected EmulatorFactory createFactory(Properties properties) {
                return new MockEmulatorFactory("test");
            }
        };

        dp.run(new ByteArrayInputStream(properties.getBytes()));
    }

}
