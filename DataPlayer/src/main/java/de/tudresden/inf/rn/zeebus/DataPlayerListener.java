/*
 * Created at 12:33:02 on 03.11.2014
 */
package de.tudresden.inf.rn.zeebus;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public interface DataPlayerListener {
    void playerFinished();
}
