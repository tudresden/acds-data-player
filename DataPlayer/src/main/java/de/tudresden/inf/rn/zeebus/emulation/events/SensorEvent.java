package de.tudresden.inf.rn.zeebus.emulation.events;

import java.text.DateFormat;
import java.text.ParseException;

/**
 * <p>
 * Generic sensor event class for every type of sensor data.
 * </p>
 * 
 * @author Philipp Grubitzsch
 * @author Klemens Muthmann
 * @version 1.0
 * @since 1.0
 */
public abstract class SensorEvent<T> {
    protected long timeToSend;

    /**
     * This Method should implemented in a way, that it returns the complete payload to transmit.
     * 
     * @return The Message payload as String.
     */
    public abstract T getMessage();

    /**
     * 
     * @return the SimulationTime when the Event has to be send
     */
    public long getTimeToSend() {
        return timeToSend;
    }

    /**
     * Method calculate the sendtime for the Event.
     * 
     * @param timeStamp
     */
    protected void calculateSendTime(String timeStamp) {
        DateFormat sdf = getDateformat();
        try {
            timeToSend = sdf.parse(timeStamp).getTime();
        } catch (ParseException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * This method should return a SimpleDateFormat Object that fits to the
     * timestamp format in the testdata scripts/logs.
     * 
     * @return the testdata specific dateformat
     */
    protected abstract DateFormat getDateformat();
}
