/*
 * Created at 12:36:02 on 07.08.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.events;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.tudresden.inf.rn.zeebus.emulation.client.AbstractClientEmulator;

/**
 * <p>
 * Queue managing all the events a {@link AbstractClientEmulator} is supposed to run. The events are stored ordered by
 * time of occurrence to ease the sequential sending of all events in the order they occurred in reality.
 * </p>
 * 
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public class EventQueue<S extends SensorEvent<?>> extends TreeMap<Long, List<S>> {
    private final static Logger LOGGER = LogManager.getLogger(EventQueue.class);
    /**
     * <p>
     * Used for serializing objects of this class. Should only be called if the properties set of this class changes.
     * </p>
     */
    private static final long serialVersionUID = -197607493236783736L;
    /**
     * <p>
     * Listeners listening on changes to this event queue.
     * </p>
     */
    private Collection<EventQueueListener> listener;

    /**
     * <p>
     * Creates a completely initialized {@code EventQueue}.
     * </p>
     */
    public EventQueue() {
        listener = new HashSet<>();
    }

    /**
     * <p>
     * provides a shallow copy of the provided {@code EventQueue}.
     * </p>
     * 
     * @param eventQueue The {@code EventQueue} to copy.
     */
    public EventQueue(EventQueue<S> eventQueue) {
        super(eventQueue);
        this.listener = eventQueue.listener;
    }

    /**
     * <p>
     * Adds a listener to this {@code EventQueue}, which is notified when the queue is empty.
     * </p>
     * 
     * @param listener The listener to add.
     */
    public void addEventQueueListener(final EventQueueListener listener) {
        this.listener.add(listener);
    }

    @Override
    public List<S> remove(Object key) {
        List<S> ret = super.remove(key);
        LOGGER.trace("Remove 1 event from event queue with size {}.", this.size());
        if (size() == 0) {
            for (EventQueueListener listener : this.listener) {
                listener.queueEmpty();
            }
        }
        return ret;
    }
}
