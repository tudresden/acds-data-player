package de.tudresden.inf.rn.zeebus.emulation.client;

import java.text.DateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.emulation.events.SensorEvent;
import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

/**
 * <p>
 * The class which handles all important functions of a single emulated Client.
 * </p>
 * 
 * @author Philipp Grubitzsch
 * @author Klemens Muthmann
 * @since 1.0
 * @version 2.0
 */
public abstract class AbstractClientEmulator<S extends SensorEvent<?>> implements ClientEmulator<S> {
    /**
     * <p>
     * The LOGGER used by objects of this class. Configure it using src/main/resources/log4j2-test.xml.
     * </p>
     */
    private static final Logger LOGGER = LogManager.getLogger(AbstractClientEmulator.class);

    private final String identifier;

    /**
     * <p>
     * Listeners listening on events from this {@link AbstractClientEmulator}.
     * </p>
     */
    private final Collection<ClientEmulatorListener> listener;
    private ZeebusLogger logger;
    /**
     * <p>
     * The current simulation time in milliseconds since epoch.
     * </p>
     */
    private long simulationTime;

    private final String testName;

    /**
     * <p>
     * Creates a new completely initialized {@link AbstractClientEmulator}.
     * </p>
     * 
     * @param eventQueue A queue containing all the events ordered by timestamp, which this client sends over the course
     *            of one simulation.
     */
    protected AbstractClientEmulator(final String identifier, final String testName, final EventQueue<S> eventQueue) {
        Validate.notEmpty(identifier);
        Validate.notNull(eventQueue);

        // this.eventQueue = eventQueue;
        eventQueue.addEventQueueListener(this);
        this.listener = new HashSet<>();
        this.identifier = identifier;
        this.testName = testName;
    }

    /**
     * <p>
     * Adds the provided listener as a listener to this {@link AbstractClientEmulator}.
     * </p>
     * 
     * @param listener The listener to add.
     */
    @Override
    public void addClientEmulatorListener(final ClientEmulatorListener listener) {
        this.listener.add(listener);
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    protected ZeebusLogger getLogger() {
        return logger;
    }

    protected String getTestName() {
        return testName;
    }

    @Override
    public void queueEmpty() {
        LOGGER.debug("Client {} has finished Simulation.", getIdentifier());
        for (ClientEmulatorListener listener : this.listener) {
            listener.emulatorFinished(this);
        }
    }

    @Override
    public void run() {
        // List<List<S>> queuePart = new LinkedList<>();
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Updating events for client {}.", getIdentifier());
            LOGGER.trace(
                    "Current simulation time {}.", DateFormat.getDateTimeInstance().format(new Date(simulationTime)));
        }
        long mostCurrentTimestamp = getEarliestEvents().getKey();
        while (!isQueueEmpty() && mostCurrentTimestamp <= simulationTime) {
            // queuePart.add(popCurrentEvents());
            List<S> events = null;
            synchronized (getEventQueue()) {
                events = removeEvents(mostCurrentTimestamp);
            }
            for (S eventToSend : events) {
                sendMessage(eventToSend);
            }
            mostCurrentTimestamp = getEarliestEvents().getKey();
        }

        // while (!queuePart.isEmpty()) {
        // List<S> list = queuePart.remove(0);
        // for (S eventToSend : list) {
        // sendMessage(eventToSend);
        // }
        // }
    }

    /**
     * @return The next event from the event queue.
     */
    protected abstract Entry<Long, List<S>> getEarliestEvents();

    /**
     * @return
     */
    protected abstract List<S> popCurrentEvents();

    /**
     * <p>
     * This method is called to send a message from a concrete protocol implementation. A pull-based mechanism like CoAP
     * would use it to update the information, that a client could pull from the sensor object.
     * </p>
     * 
     * @param event The event to send when calling this method
     */
    protected abstract void sendMessage(S event);

    @Override
    public void setCurrentSimulationTime(final long simulationTime) {
        Validate.isTrue(simulationTime > this.simulationTime);
        this.simulationTime = simulationTime;
    }

    @Override
    public void setLogger(final ZeebusLogger logger) {
        this.logger = logger;
    }

    @Override
    public void shutdown() {
        if (logger != null) {
            logger.shutDown();
        }
    }

    @Override
    public void reduceEventQueue(long simulationStartTime, long simulationEndTime) {

        // make a copy to avoid ConcurrentModificationException
        EventQueue<S> eventQueue = new EventQueue<>(getEventQueue());
        for (Entry<Long, List<S>> eventsAtTime : eventQueue.entrySet()) {

            // check if event has a timestamp after simulation ending
            // time and check if event has a timestamp before simulations
            // starting time
            long timestamp = eventsAtTime.getKey();
            if (timestamp >= simulationEndTime || timestamp < simulationStartTime) {
                removeEvents(timestamp);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            StringBuilder sb = new StringBuilder();
            for (List<S> evs : getEventQueue().values()) {
                for (S ev : evs) {
                    sb.append(ev.getMessage());
                }
            }
            LOGGER.debug(sb.toString());
        }
    }

    protected abstract List<S> removeEvents(final long timestamp);
}
