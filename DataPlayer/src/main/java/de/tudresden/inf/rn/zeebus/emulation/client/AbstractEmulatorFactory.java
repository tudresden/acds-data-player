/*
 * Created at 12:42:23 on 25.08.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.client;

import java.io.File;

import org.apache.commons.lang3.Validate;

import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class AbstractEmulatorFactory implements EmulatorFactory {

    private final String testName;
    private String logPath;
    private final static String LOG_FILE_HEADER = "id;time;fromjid;tojid;msg;msgsize;pldsize";

    public AbstractEmulatorFactory(final String testName) {
        Validate.notEmpty(testName);

        this.testName = testName;
        this.logPath = "logs" + File.separator + testName + File.separator;
    }

    public String getTestName() {
        return testName;
    }

    public void setLogPath(final String logPath) {
        Validate.notEmpty(logPath);

        this.logPath = logPath;
    }
    
    /**
     * @param identifier the id of the ClientEmulator using this logger
     * @param label determine the folder for different kinds of Logger Entities like sender, receiver etc.
     * @return
     */
    protected final ZeebusLogger createLogger(final String identifier, final String label) {
        return new ZeebusLogger(logPath + label + File.separator + identifier + ".csv", LOG_FILE_HEADER);
    }

    public String getLogPath() {
        return logPath;
    }

}
