/*
 * Created at 15:34:32 on 29.07.2014
 */
package de.tudresden.inf.rn.zeebus.simulation;

/**
 * <p>
 * Listener for {@link SimulationTimer}s. Is notified about timer events if registered with the timer using
 * {@link SimulationTimer#addTimerListener(TimerListener)}.
 * </p>
 * 
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public interface TimerListener {

    /**
     * <p>
     * Called once when the {@link SimulationTimer} has been started.
     * </p>
     * 
     * @param timer The {@link SimulationTimer} that was started.
     */
    void timerStarted(SimulationTimer timer);

    /**
     * <p>
     * Called once when the {@link SimulationTimer} has stopped running.
     * </p>
     */
    void timerFinished();

    /**
     * <p>
     * Called on every tick of the {@link SimulationTimer}. This method should contain the actual business logic that
     * needs to be carried out.
     * </p>
     * 
     * @param tickTime Current simulation time the tick occured on.
     */
    void tick(long tickTime);

}
