/*
 * Created at 12:53:28 on 18.08.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.client;

import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;

/**
 * <p>
 * Interface for objects listening to events from a {@link AbstractClientEmulator}.
 * </p>
 * 
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public interface ClientEmulatorListener {
    /**
     * <p>
     * Called when the {@link AbstractClientEmulator} has finished processing its {@link EventQueue}.
     * </p>
     * 
     * @param emulator The {@link AbstractClientEmulator} which has finished.
     */
    void emulatorFinished(ClientEmulator<?> emulator);
}
