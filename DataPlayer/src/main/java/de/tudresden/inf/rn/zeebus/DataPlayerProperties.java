/*
 * Created at 09:55:31 on 12.09.2014
 */
package de.tudresden.inf.rn.zeebus;

import java.io.File;

import de.tudresden.inf.rn.zeebus.emulation.client.Logging;

public class DataPlayerProperties {

    private String testName;
    private File scriptFileFolderPath;
    private String scenarioDescriptionFile = "/simple_test-scenario.xml";
    private String simulationType;
    private int userPerSender;
    private String userHost;
    private String senderHost;
    private int numberOfClients;
    private int emulationThreadPoolSize;
    // private String federatedUserHost;
    private boolean isFederated;
    private String logFolder;
    private Logging isLoggingActive;
    private int port;
    private long simulatedMillisecondsPerTick;
    private boolean isGuiEnabled;
    private long simulationInterval;
    private long simulationStartDelay;
    private long simulationDuration;
    private long simulatedEndTime;
    private long startTime;
    private long simulatedStartTime;

    // private String clientHost;

    public Logging isLoggingActive() {
        return isLoggingActive;
    }

    public boolean isGuiEnabled() {
        return isGuiEnabled;
    }

    public long getSimulationStartDelay() {
        return simulationStartDelay;
    }

    public int getEmulationThreadPoolSize() {
        return emulationThreadPoolSize;
    }

    public void setTestName(final String testName) {
        this.testName = testName;
    }

    public long getSimulationDuration() {
        return simulationDuration;
    }

    public long getSimulatedMillisecondsPerTick() {
        return simulatedMillisecondsPerTick;
    }

    public long getSimulationInterval() {
        return simulationInterval;
    }

    public long getRealStartTime() {
        return startTime;
    }

    public long getSimulatedEndTime() {
        return simulatedEndTime;
    }

    public long getSimulatedStartTime() {
        return simulatedStartTime;
    }

    public File getScriptFileFolderPath() {
        return scriptFileFolderPath;
    }

    public String getScenarioDescriptionFilePath() {
        return scriptFileFolderPath + scenarioDescriptionFile;
    }

    public String getSimulationType() {
        return simulationType;
    }

    public int getUserPerSender() {
        return userPerSender;
    }

    public String getUserHost() {
        return userHost;
    }

    public String getSenderHost() {
        return senderHost;
    }

    public File[] loadScripts() {
        File scriptsPath = getScriptFileFolderPath();

        if (!scriptsPath.exists()) {
            throw new IllegalStateException("Scripts path does not exists. Please specify a valid path.");
        }

        return scriptsPath.listFiles(new ScriptsBySizeFilter(scriptsPath, getClientsNumber()));
    }

    private int getClientsNumber() {
        return numberOfClients;
    }

    public String getTestName() {
        return testName;
    }

    public void setEmulationThreadPoolSize(final int emulationThreadPoolSize) {
        this.emulationThreadPoolSize = emulationThreadPoolSize;
    }

    public void setUserHost(final String userHost) {
        this.userHost = userHost;
    }

    public void setFederatedStatus(boolean isFederated) {
        this.isFederated = isFederated;
    }

    public void setLogFolder(final String logFolder) {
        this.logFolder = logFolder;
    }

    public void setLoggingActiveStatus(final boolean isLoggingActive) {
        this.isLoggingActive = isLoggingActive ? Logging.ACTIVE : Logging.INACTIVE;
    }

    public void setUsersPerSender(final int userPerSender) {
        this.userPerSender = userPerSender;
    }

    public void setPort(final int port) {
        this.port = port;
    }

    public void setSenderHost(final String senderHost) {
        this.senderHost = senderHost;
    }

    public void setSimulationType(final String simulationType) {
        this.simulationType = simulationType;
    }

    public void setGuiEnabledStatus(final boolean isGuiEnabled) {
        this.isGuiEnabled = isGuiEnabled;
    }

    public void setNumberOfClients(final int numberOfClients) {
        this.numberOfClients = numberOfClients;
    }

    public void setScriptFolder(final String scriptFolderPathName) {
        this.scriptFileFolderPath = new File(scriptFolderPathName);
    }

    public void setRealMillisecondsPerTick(final long realMillisecondsPerTick) {
        this.simulatedMillisecondsPerTick = realMillisecondsPerTick;
    }

    public void setSimulationInterval(final long simulationInterval) {
        this.simulationInterval = simulationInterval;
    }

    public void setSimulationStartDelay(final long simulationStartDelay) {
        this.simulationStartDelay = simulationStartDelay;
    }

    public void setSimulationDuration(final long simulationDuration) {
        this.simulationDuration = simulationDuration;
    }

    public void setSimulatedEndTime(final long simulatedEndTime) {
        this.simulatedEndTime = simulatedEndTime;
    }

    public void setStartTime(final long startTime) {
        this.startTime = startTime;
    }

    public void setSimulatedStartingTime(final long simulatedStartingTime) {
        this.simulatedStartTime = simulatedStartingTime;
    }

}