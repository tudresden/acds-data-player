package de.tudresden.inf.rn.zeebus;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ScriptsBySizeFilter implements FileFilter {

    private static final Logger LOGGER = LogManager.getLogger(ScriptsBySizeFilter.class);
    ArrayList<File> scripts;

    public ScriptsBySizeFilter(final File path_Scripts, final int clientsNumberRequested) {
        calculateFilesBySize(clientsNumberRequested, path_Scripts);
    }

    @Override
    public boolean accept(final File file) {
        if (scripts.contains(file)) {
            return true;
        } else
            return false;
    }

    private void calculateFilesBySize(final int clientsNumberRequested, final File scriptsPath) {
        File[] file = scriptsPath.listFiles();
        List<File> filesBySize = new ArrayList<File>();
        int clientsNumber;
        if (file.length >= clientsNumberRequested) {
            clientsNumber = clientsNumberRequested;
        } else {
            clientsNumber = file.length;
            LOGGER.warn(
                    "Can't build the requested Number of {} clients cause of not enough Script files. Building {} clients from available script files instead...",
                    clientsNumberRequested, clientsNumber);
        }

        filesBySize.addAll(Arrays.asList(file));
        Collections.sort(filesBySize, new Comparator<File>() {

            @Override
            public int compare(File o1, File o2) {
                if (o1.length() > o2.length()) {
                    return -1;
                } else if (o1.length() < o2.length()) {
                    return 1;
                } else {
                    return 0;
                }
            }

        });
        scripts = new ArrayList<File>();
        for (int i = 0; i < clientsNumber; i++) {
            LOGGER.trace(filesBySize.get(i).getAbsolutePath());
            scripts.add(filesBySize.get(i));
        }
    }

}
