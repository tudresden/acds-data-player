package de.tudresden.inf.rn.zeebus.gui;

import java.awt.BorderLayout;
import java.text.DateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JLabel;

import de.tudresden.inf.rn.zeebus.simulation.SimulationTimer;
import de.tudresden.inf.rn.zeebus.simulation.TimerListener;

/**
 * <p>
 * Init the Main Window of the GUI.
 * </p>
 * 
 * @author Philipp Grubitzsch
 * @author Klemens Muthmann
 * @since 1.0
 * @version 1.0
 */
public final class MainWindow extends JFrame implements TimerListener, Runnable {
    private static final long serialVersionUID = 1L;
    private static MainWindow instance;
    private Date timestamp;
    private static final DateFormat DF = DateFormat.getDateTimeInstance();
    /**
     * <p>
     * A label for showing the current simulation time of the simulation presented on this GUI.
     * </p>
     */
    private final JLabel timerLabel;

    public MainWindow() {
        super();
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        timerLabel = new JLabel("Hallo Welt", JLabel.CENTER);
        timestamp = new Date();
        this.add(timerLabel);
        this.getContentPane().add(timerLabel, BorderLayout.PAGE_START);
    }

    public static MainWindow getMainWindow() {
        if (instance == null) {
            instance = new MainWindow();
        }
        return instance;
    }

    @Override
    public void tick(final long timestamp) {
        this.timestamp.setTime(timestamp);
        timerLabel.setText(this.timestamp.toString());
        // SwingUtilities.invokeLater(this);

    }

    @Override
    public void timerFinished() {
        // TODO Auto-generated method stub

    }

    @Override
    public void timerStarted(final SimulationTimer timer) {
        this.timestamp.setTime(timer.getSimulatedStartTime());
        timerLabel.setText(timestamp.toString());
        // SwingUtilities.invokeLater(this);

    }

    @Override
    public void run() {
        timerLabel.setText(timestamp.toString());
    }
}
