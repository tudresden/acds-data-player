/*
 * Created at 14:09:37 on 18.08.2014
 */
package de.tudresden.inf.rn.zeebus.emulation;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public interface EmulationControllerListener {
    void finishedEmulation();
}
