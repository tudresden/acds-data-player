/*
 * Created at 09:23:15 on 07.08.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.client;

import java.util.Collection;
import java.util.concurrent.Callable;

import de.tudresden.inf.rn.zeebus.emulation.server.ServerEmulator;
import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;

/**
 * <p>
 * Handles the creation of a new {@link AbstractClientEmulator}. This class implements a {@link Callable} so that it is
 * called asynchronously, which enables us to create multiple clients in parallel speeding up the initialization phase
 * of a new simulation.
 * </p>
 * 
 * @author Klemens Muthmann
 *
 * @version 2.0
 * @since 1.0
 */
public interface EmulatorFactory {
    // /**
    // * <p>
    // * The LOGGER used by objects of this class. Configure it using src/main/resources/log4j2-text.xml.
    // * </p>
    // */
    // private static final Logger LOGGER = LogManager.getLogger(EmulatorFactory.class);

    // private final ClientFactory<T> protocolImplementationFactory;
    //
    // /**
    // * <p>
    // * Responsible for filling the new {@link AbstractClientEmulator}s initial {@link EventQueue}.
    // * </p>
    // */
    // private final EventQueueFactory<T, ? extends SensorEvent<T>> eventCreator;
    //
    // private final SimulationConfig configuration;
    // private final Logging logging;

    // /**
    // * <p>
    // * Creates a new completely initialized {@code EmulatorFactory}.
    // * </p>
    // */
    // public EmulatorFactory(final ClientFactory<T> protocolImplementationFactory,
    // final EventQueueFactory<T, ? extends SensorEvent<T>> eventCreator,
    // final SimulationConfig configuration, final Logging logging) {
    // Validate.notNull(protocolImplementationFactory);
    // Validate.notNull(eventCreator);
    //
    // this.protocolImplementationFactory = protocolImplementationFactory;
    // this.eventCreator = eventCreator;
    // this.configuration = configuration;
    // this.logging = logging;
    // }

    // @Override
    // public AbstractClientEmulator<T> call() {
    // // get eventQueue (not streamed yet)
    // ProtocolImplementation<T> protocol = protocolImplementationFactory.createProtocolImplementation(
    // configuration, logging);
    // EventQueue<? extends SensorEvent<T>> eventQueue = eventCreator.createEventQueue(configuration);
    // LOGGER.debug("{}: Event Queue filled with {} events.", this.toString(), eventQueue.size());
    // protocol.connect();
    //
    // // get protocol implementation (with configuration in dependency of
    // // source data)
    // // protocolImplemenation = ConnectionFactory.getProtocolImplementation(ConfigFactory.getConfig(eventFactory
    // // .getSendersInformation()));
    // if (!eventQueue.isEmpty()) {
    // return new AbstractClientEmulator<>(protocol, eventQueue);
    // } else
    // return null;
    //
    // // controller.clientBuilt(product);
    // }

    Collection<? extends ClientEmulator<?>> createClients(Logging logging);

    Collection<ServerEmulator> createServers(Logging logging);

    Collection<UserEmulator> createUsers(Logging logging);

}
