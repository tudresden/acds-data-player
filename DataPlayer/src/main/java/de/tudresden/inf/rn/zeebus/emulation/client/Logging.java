/*
 * Created at 16:42:03 on 25.08.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.client;

public enum Logging {
    ACTIVE, INACTIVE;
}