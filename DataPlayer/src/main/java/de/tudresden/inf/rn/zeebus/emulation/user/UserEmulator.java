package de.tudresden.inf.rn.zeebus.emulation.user;

import java.util.concurrent.Callable;

import org.apache.commons.lang3.Validate;

import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

/**
 * <p>
 * Abstract base class for data player receivers, waiting for the reponses of messages send by {@code ClientEmulators}.
 * </p>
 * 
 * @author Philipp Grubitzsch
 * @author Klemens Muthmann
 * @version 1.0
 * @since 1.0
 */
public abstract class UserEmulator implements Callable<UserEmulator> {

    public enum Mode {
        INIT, SHUTDOWN
    }

    public Mode mRunMode;
    private final String testName;
    private ZeebusLogger logger;

    protected abstract void init();

    protected void shutdown() {
        if (logger != null) {
            logger.shutDown();
        }
    }

    /**
     * 
     * @param receiverController
     * @param logging
     */
    protected UserEmulator(final String testName) {
        this.testName = testName;
    }

    /**
     * Method for threaded processing of the receiver.
     */
    public UserEmulator call() {
        switch (mRunMode) {

            case INIT:
                init();
                break;

            case SHUTDOWN:
                shutdown();
                break;
        }
        return this;
    }

    /**
     * Method should return a String that describe the entity, e.g. the JID for an XMPP receiver.
     * 
     * @return object_description
     */
    @Override
    public String toString() {
        throw new UnsupportedOperationException(
                "[ERROR] UserEmulator.toString() needs to be overwritten by concrete receivers");
    }

    /**
     * Returns either INIT or SHUTDOWN.
     * 
     * @return
     */
    public Mode getMode() {
        return mRunMode;
    }

    /**
     * Sets either INIT or SHUTDOWN.
     * 
     * @param mode
     */
    public void setMode(Mode mode) {
        this.mRunMode = mode;
    }

    protected ZeebusLogger getLogger() {
        return logger;
    }

    public final void setLogger(final ZeebusLogger logger) {
        Validate.notNull(logger);
        this.logger = logger;
    }

    /**
     * @return the testName
     */
    public String getTestName() {
        return testName;
    }
}
