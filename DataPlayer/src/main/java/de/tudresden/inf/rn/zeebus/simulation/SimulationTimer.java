package de.tudresden.inf.rn.zeebus.simulation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * <p>
 * This class is a time simulator that informs listening observers about the actual progress of a simulation in fixed
 * intervals.
 * </p>
 * <p>
 * The concepts used here can be a little hard to get. You need to know that there is a simulation time that needs to
 * run within the bounds of real time. As the name suggests the real time is the time as you experience it, so 1
 * millisecond in real time is 1 millisecond for you. The simulation time however may run faster or slower depending on
 * the settings you provide to the constructor of this class. You also can increase or decrease the resolution of the
 * simulation by setting the interval for the simulation.
 * </p>
 * 
 * @author Philipp Grubitzsch
 * @author Klemens Muthmann
 * @version 1.0
 * @since 1.0
 */
public class SimulationTimer {
    private static final Logger LOGGER = LogManager.getLogger(SimulationTimer.class.getName());

    /**
     * <p>
     * The simulated time the simulation is supposed to start with in milliseconds since epoch. This date might be in
     * the past or in the future.
     * </p>
     */
    private long simulatedStartTime; // the the simulated start date, e.g. 22.10.1995 -
    // 10:23

    /**
     * <p>
     * The simulated end time for the simulation in milliseconds since epoch.
     * </p>
     */
    private final long simulatedEndTime;
    /**
     * <p>
     * The real start time for the simulation when the simulation is supposed to start. You may set this time to now
     * (e.g. {@code Calendar#getInstance()} and {@code Calendar#getTimeInMillis()}) if you need to start the simulation
     * immediately.
     * </p>
     */
    private final long realStartTime;
    /**
     * <p>
     * The interval of how much real time passes between two ticks of the timer.
     * </p>
     */
    private final long simulationInterval;
    /**
     * <p>
     * The number of simulated milliseconds that should be handled with each tick. If you set this value to 1000, for
     * example, each tick will be 1 second.
     * </p>
     */
    private final long simulatedMillisecondsPerTick;

    private Timer timer; // internal reference clock to calculate the simulatedStartTime

    private Collection<TimerListener> listener;

    private boolean hasBeenStopped;

    /**
     * <p>
     * Creates a new completely initialized object of this class. You may start the timer by calling
     * {@code #startSimulation(Date)}.
     * </p>
     * 
     * @param simulatedStartTime The simulated time the simulation is supposed to start with in milliseconds since
     *            epoch.
     * @param simulatedEndTime The simulated end time for the simulation in milliseconds since epoch.
     * @param simulationDuration The maximal duration of the simulation in seconds of simulation time. If this duration
     *            would end before {@code simulatedEndTime} the {@code simulatedEndTime} is corrected to match this
     *            duration.
     * @param realStartTime The real start time for the simulation when the simulation is supposed to start. You may set
     *            this time to now (e.g. {@code Calendar#getInstance()} and {@code Calendar#getTimeInMillis()}) if you
     *            need to start the simulation immediately.
     * @param simulationInterval The interval of how much real time passes between two ticks of the timer. There will be
     *            one tick every simulationInterval milliseconds.
     * @param simulatedMillisecondsPerTick The number of simulated milliseconds that should pass with each tick.
     *            If you
     *            set this value to 1000, for example, each tick will be worth 1 second.
     */
    public SimulationTimer(final long simulatedStartTime, final long simulatedEndTime, final long simulationDuration,
                           final long realStartTime, final long simulationInterval,
                           final long simulatedMillisecondsPerTick) {
        this.simulatedStartTime = simulatedStartTime; // the simulated start date, e.g. 22.10.1995 - 10:23
        this.simulatedMillisecondsPerTick = simulatedMillisecondsPerTick;
        this.realStartTime = realStartTime;
        this.simulationInterval = simulationInterval;
        this.simulatedEndTime = calcEndOfSimTime(simulatedEndTime, simulationDuration);
        this.listener = new HashSet<>();
        this.hasBeenStopped = false;

        if (LOGGER.isInfoEnabled()) {
            SimpleDateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
            LOGGER.info(
                    "Created timer with simulation start time {} and end time {}",
                    format.format(new Date(simulatedStartTime)), format.format(new Date(this.simulatedEndTime)));
        }
    }

    /**
     * <p>
     * Count up the SimulationTime another Tic. A Tic is the set minimum time unit and represents the time resolution of
     * the Simulation.
     * </p>
     * 
     * @author Philipp Grubitzsch
     * @version 1.0
     * @since 1.0
     */
    private class NextTick extends TimerTask {

        @Override
        public void run() {
            LOGGER.trace(
                    "Tick at {}.",
                    DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.MEDIUM).format(
                            new Date(simulatedStartTime)));
            simulatedStartTime += simulatedMillisecondsPerTick;
            long logtime = simulatedStartTime;
            tick(logtime);
            if (logtime > simulatedEndTime) {
                stop();
                LOGGER.info("Simulation end time reached.");
            }
        }
    }

    public synchronized void stop() {
        if (!hasBeenStopped) {
            hasBeenStopped = true;
            this.timer.cancel();
            for (final TimerListener listener : this.listener) {
                listener.timerFinished();
            }
        }
    }

    /**
     * <p>
     * Starts this timer with a minimum delay at the time specified via {@link #realStartTime} in the constructor
     * {@link #SimulationTimer(long, long, long, long, long, long)}. This means if there is not enough time left until
     * {@link #realStartTime} to satisfy the min delay, the real start of the simulation is postponed until min delay
     * time has passed. If there is enough time until the real start time left the simulation starts at the real start
     * time even if min delay is shorter.
     * </p>
     * 
     * @param minDelay The minimum delay in seconds to start the timer on.
     */
    public void start(final long minDelay) {
        Date scheduledStart = new Date(calcSimulationStartTime(minDelay));
        this.timer = new Timer();
        if (LOGGER.isDebugEnabled()) {
            DateFormat dateTimeFormatter = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.MEDIUM);
            LOGGER.debug(
                    "Running simulation from {} to {} with an interval of {} ms and one tick every {} ms of real time, starting at {}.",
                    dateTimeFormatter.format(new Date(simulatedStartTime)),
                    dateTimeFormatter.format(new Date(simulatedEndTime)), simulatedMillisecondsPerTick,
                    this.simulationInterval, dateTimeFormatter.format(scheduledStart));
        }

        for (TimerListener listener : this.listener) {
            listener.timerStarted(this);
        }

        this.timer.scheduleAtFixedRate(new NextTick(), scheduledStart, this.simulationInterval);

        if (LOGGER.isInfoEnabled()) {
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            long acceleration = simulatedMillisecondsPerTick / simulationInterval;

            // print out setup information
            String testInfo = "\n--------------------------------------------------------------------------------\n"
                    + "Starting Simulation at: " + sdf2.format(realStartTime) + "\nsimulated startTime: "
                    + sdf2.format(simulatedStartTime) + "\ntime accelleration: " + acceleration + "x\n"
                    + "Simulation time: " + (simulatedEndTime - simulatedStartTime) + " seconds\n"
                    + "simulated endTime: " + sdf2.format(simulatedEndTime)
                    + "\n--------------------------------------------------------------------------------\n";
            LOGGER.info(testInfo);
        }
    }

    /**
     * <p>
     * Calculates the end for the simulation time based on the end time of the simulation and a duration time. If the
     * difference between the simulated end time and the simulated start time is not as large as the duration, the end
     * time is pushed to satisfy the simulation duration.
     * </p>
     * 
     * @param simulatedEndTime The end time for the simulation in milliseconds since epoch.
     * @param simulationDuration The duration of the simulation in simulation time.
     * @return
     */
    private long calcEndOfSimTime(final long simulatedEndTime, final long simulationDuration) {
        if (simulationDuration <= 0l) {
            return simulatedEndTime;
        }

        // long correctedEndTime = this.simulatedStartTime + (simulationDuration * 1000l)
        // * (this.realMillisecondsPerTick / this.simulationInterval);
        long correctedEndTime = this.simulatedStartTime + this.simulatedMillisecondsPerTick * simulationDuration
                * 1000l / simulationInterval;
        if (correctedEndTime > this.simulatedStartTime && correctedEndTime < simulatedEndTime) {
            return correctedEndTime;
        } else if (correctedEndTime > simulatedEndTime) {
            return simulatedEndTime;
        } else {
            throw new IllegalArgumentException(
                    "Unable to calculate end time. Please provide either an end time for the simulation or a duration.");
        }
    }

    /**
     * <p>
     * Calculates the true start time for the simulation based on a minimum delay, which must occur. This is usually
     * required to wait for the system to finish setup, which might take some time due to network latency or parallel
     * slow initialization procedures.
     * </p>
     * 
     * @param minDelay The minimum delay in seconds which needs to occur before the simulation starts.
     * @return The true start time in milliseconds since epoch.
     */
    private long calcSimulationStartTime(final long minDelay) {
        // check if minDelay of starting time for the simulation is valid
        if (this.realStartTime < System.currentTimeMillis() + minDelay * 1000l) {
            return new Date().getTime() + minDelay * 1000l;
        }
        return this.realStartTime;
    }

    public void addTimerListener(final TimerListener listener) {
        this.listener.add(listener);
    }

    public void removeTimerListener(TimerListener listener) {
        this.listener.remove(listener);
    }

    private void tick(final long tickTime) {
        for (TimerListener listener : this.listener) {
            listener.tick(tickTime);
        }
    }

    public long getSimulatedEndTime() {
        return simulatedEndTime;
    }

    public long getSimulatedStartTime() {
        return simulatedStartTime;
    }

    public Date getRealStartTime() {
        return new Date(realStartTime);
    }

    public long getSimulatedDuration() {
        return simulatedEndTime - simulatedStartTime;
    }

    public long getSimulationInterval() {
        return simulationInterval;
    }

    public void pause() {
        // TODO implement this sucker
    }

}
