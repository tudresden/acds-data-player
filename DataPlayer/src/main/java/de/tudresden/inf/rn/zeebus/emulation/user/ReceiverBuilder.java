/*
 * Created at 17:37:02 on 21.08.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.user;

import java.util.List;
import java.util.concurrent.Future;

import de.tudresden.inf.rn.zeebus.emulation.client.Logging;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public interface ReceiverBuilder {

    void testDiscoveryTime();

    List<String> discoverTopics();

    List<Future<UserEmulator>> createReceivers(List<String> topics, Logging logging);

    void shutdown();

}
