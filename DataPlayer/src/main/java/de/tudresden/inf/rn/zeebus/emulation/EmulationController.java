package de.tudresden.inf.rn.zeebus.emulation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.tudresden.inf.rn.zeebus.DataPlayer;
import de.tudresden.inf.rn.zeebus.emulation.client.ClientEmulator;
import de.tudresden.inf.rn.zeebus.emulation.client.ClientEmulatorListener;
import de.tudresden.inf.rn.zeebus.emulation.client.EmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.Logging;
import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;
import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator.Mode;
import de.tudresden.inf.rn.zeebus.simulation.SimulationTimer;
import de.tudresden.inf.rn.zeebus.simulation.TimerListener;

/**
 * <p>
 * An object of this class controls a complete set of senders and receivers on the same physical machine. It is
 * responsible for running the complete workflow of setting up senders and receivers, starting the simulation and
 * shutting everything down again.
 * </p>
 * 
 * @author Philipp Grubitzsch
 * @author Klemens Muthmann
 * @version 1.0
 * @since 1.0
 * 
 */
public class EmulationController implements TimerListener, ClientEmulatorListener {

    /**
     * <p>
     * A {@link TimerTask} for a controlled shutdown of all {@link ClientEmulator}s at the end of a {@link DataPlayer}
     * run.
     * </p>
     * 
     * @author Philipp Grubitzsch
     * @author Klemens Muthmann
     *
     * @version 1.0
     * @since 1.0
     */
    private class ShutdownAllClients extends TimerTask {
        /**
         * <p>
         * The {@link Timer} used to run this task, required to run this task only once and shut it down immediately.
         * </p>
         */
        Timer timer;

        /**
         * <p>
         * Creates a new completely initialized task.
         * </p>
         * 
         * @param timer The {@link Timer} used to run this task, required to run this task only once and shut it down
         *            immediately.
         */
        public ShutdownAllClients(Timer timer) {
            this.timer = timer;
        }

        @Override
        public void run() {
            LOGGER.info("Shutdown all Clients...");
            // runningClients = clients.size();
            Collection<ClientEmulator<?>> clientsCopy = new ArrayList<>(clients);

            for (ClientEmulator<?> client : clientsCopy) {
                client.shutdown();

                // updateClientsCount(client);
                // shutdownExecutors.execute(client);
            }
            LOGGER.info("Shutdown all Receivers...");
            for (UserEmulator user : users) {
                LOGGER.info("Shutdown user: {}", user.toString());
                user.setMode(Mode.SHUTDOWN);
                eventExecutors.submit(user);
            }
            timer.cancel();
            eventExecutors.shutdown();

            if (callback != null) {
                callback.finishedEmulation();
            }
        }

    }

    /**
     * <p>
     * Thread pool for processing the event queue of each controlled sender.
     * </p>
     */
    private ExecutorService eventExecutors;
    /**
     * <p>
     * The LOGGER used by objects of this class.
     * </p>
     */
    private static final Logger LOGGER = LogManager.getLogger(EmulationController.class);

    /**
     * Returns the thread pool for executing Events during the simulation.
     * 
     * @return ExecutorService EventExecutingThreadpool
     */
    protected ExecutorService getEventProcessingThreadPool() {
        return eventExecutors;
    }

    /**
     * <p>
     * A callback listening for events on this {@code EmulationController}.
     * </p>
     */
    private EmulationControllerListener callback;
    /**
     * <p>
     * The sender clients controlled by this {@link EmulationController}.
     * </p>
     */
    private List<ClientEmulator<?>> clients;

    /**
     * <p>
     * A flag specifying the status of whether application logging is active or not. This is not to be confused with the
     * development logging provided via log4j. Application logging is required to measure the protocol the current run
     * has tested.
     * </p>
     */
    private final Logging logging;
    /**
     * <p>
     * The list of currently running clients. As soon as the lists size reaches zero the simulation is shutting down.
     * </p>
     */
    private List<ClientEmulator<?>> runningClients;
    /**
     * <p>
     * The timer used as heartbeat for this {@link EmulationController}. Each tick of this timer the emulation makes
     * another step.
     * </p>
     */
    private SimulationTimer timer;

    /**
     * <p>
     * The users receiving messages from the senders in {@link #clients}.
     * </p>
     */
    private List<UserEmulator> users;

    /**
     * <p>
     * Creates a completely initialized {@code EmulationController}.
     * </p>
     * 
     * @param threadsToUse The amount of threads to use for running the emulated clients.
     * @param simulationTimer A flag specifying the status of whether application logging is active or not. This is not
     *            to be confused with the
     *            development logging provided via log4j. Application logging is required to measure the protocol the
     *            current run
     *            has tested.
     */
    public EmulationController(final int threadsToUse, final Logging logging) {
        clients = new ArrayList<>();
        users = new ArrayList<>();
        runningClients = new CopyOnWriteArrayList<>();
        eventExecutors = Executors.newFixedThreadPool(threadsToUse);
        this.logging = logging;
    }

    public void deploy(final EmulatorFactory factory) {
        Validate.notNull(factory);

        clients.addAll(factory.createClients(logging));
        users.addAll(factory.createUsers(logging));
    }

    @Override
    public synchronized void emulatorFinished(ClientEmulator<?> emulator) {
        runningClients.remove(emulator);
        LOGGER.info("Clients still running {}", runningClients.size());
        if (runningClients.isEmpty()) {
            if (timer != null) { // if initial queue size is 0, timer may not have been set.
                timer.stop();
            }
        }
    }

    public void setEmulationControllerListener(EmulationControllerListener listener) {
        this.callback = listener;
    }

    @Override
    public void tick(long tickTime) {
        for (ClientEmulator<?> emulator : runningClients) {
            emulator.setCurrentSimulationTime(tickTime);
            eventExecutors.submit(emulator);
        }
    }

    @Override
    public void timerFinished() {
        while (runningClients.size() > 0) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Timer finished. Waiting for last clients ...");
                LOGGER.debug("Still running clients {}", runningClients.size());
                for (ClientEmulator<?> emulator : clients) {
                    LOGGER.debug(
                            "Queue size is {} for client {}", emulator.getEventQueue().entrySet().size(),
                            emulator.getIdentifier());
                }
            }
            try {
                Thread.sleep(1000l);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }

        Timer end = new Timer();
        LOGGER.info("Start shutdown in 5 Seconds...");
        end.schedule(new ShutdownAllClients(end), 5000);
    }

    @Override
    public void timerStarted(final SimulationTimer timer) {
        ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);
        try {
            Collection<Future<?>> futures = new HashSet<>();
            for (final ClientEmulator<?> client : clients) {
                futures.add(threadPool.submit(new Runnable() {

                    @Override
                    public void run() {
                        runningClients.add(client);
                        client.addClientEmulatorListener(EmulationController.this);
                        client.reduceEventQueue(timer.getSimulatedStartTime(), timer.getSimulatedEndTime());
                        client.connect();
                    }
                }));

            }

            for (Future<?> future : futures) {
                try {
                    future.get();
                } catch (InterruptedException | ExecutionException e) {
                    throw new IllegalStateException(e);
                }
            }
            this.timer = timer;
        } finally {
            threadPool.shutdown();
        }
    }
}
