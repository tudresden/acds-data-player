package de.tudresden.inf.rn.zeebus;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.tudresden.inf.rn.zeebus.emulation.EmulationController;
import de.tudresden.inf.rn.zeebus.emulation.client.ClientEmulator;
import de.tudresden.inf.rn.zeebus.emulation.client.EmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.Logging;
import de.tudresden.inf.rn.zeebus.gui.MainWindow;
import de.tudresden.inf.rn.zeebus.simulation.SimulationTimer;
import de.tudresden.inf.rn.zeebus.simulation.TimerListener;

/**
 * <p>
 * Central class for the data player scenario. This class is capable of starting to play data based on input properties.
 * Subclasses are used to define the scenario for the data to play. There are a few properties required for all
 * scenarios. All other properties are described in the corresponding subclass. The shared properties are:
 * </p>
 * <dl>
 * <dt>simulatedStartTime</dt>
 * <dd>The simulated time the simulation is supposed to start with in milliseconds since epoch. This value is mandatory
 * and has no default value.</dd>
 * <dt>simulatedEndingTime</dt>
 * <dd>The simulated end time for the simulation in milliseconds since epoch. This value is mandatory and has no default
 * value.</dd>
 * <dt>startOfSimulation</dt>
 * <dd>The real start time for the simulation when the simulation is supposed to start. Default value is the current
 * time.</dd>
 * <dt>simRefreshRate</dt>
 * <dd>The interval of how much real time passes between two ticks of the timer. There will be one tick every
 * simulationInterval milliseconds. This value is mandatory and has no default value.</dd>
 * <dt>timeResolution</dt>
 * <dd>The number of simulated milliseconds that should pass with each tick. If you set this value to 1000, for example,
 * each tick will be worth 1 second. This value is mandatory and has no default value.</dd>
 * <dt>simulationTime</dt>
 * <dd>The maximal duration of the simulation in seconds of simulation time. This value might conflict with the
 * simulated end time. If this duration would end before {@code simulatedEndingTime} the {@code simulatedEndingTime} is
 * corrected to match this duration. However if this duration is 0 it is completely ignored and the
 * {@code simulatedEndingTime} is used instead. Default value 0.</dd>
 * <dt>threads</dt>
 * <dd>The maximum size for the thread pool to use to emulate clients. Default value is the number of processors
 * assigned to this virtual machine.</dd>
 * <dt>logging</dt>
 * <dd>Either {@code true} or {@code false}. If {@code true} the emulated clients will provide a detailed log about
 * their doings in the "logs" folder. If {@code false} no log is written. Default value is {@code false}.</dd>
 * <dt>minDelay</dt>
 * <dd>The minimum amount of time to wait before the simulation starts in seconds. Default value is 0.</dd>
 * <dt>gui</dt>
 * <dd>Either {@code true} or {@code false}. If {@code true} a small graphical progress report window will appear, if
 * {@code false} no gui will be shown. Default value is {@code false}.</dd>
 * </dl>
 *
 * @author Philipp Grubitzsch
 * @author Klemens Muthmann
 * @version 3.0
 * @since 1.0
 */
public abstract class DataPlayer {
    // Parameters in config file
    /**
     * <p>
     * Constant for the string describing the time between two ticks of the simulation heart beat.
     * </p>
     * 
     * @see SimulationTimer
     */
    private static final String SIMULATION_INTERVAL = "simRefreshRate";
    /**
     * <p>
     * Constant for the string describing the speed up of the simulation.
     * </p>
     * 
     * @see SimulationTimer
     */
    private static final String SIMULATED_MILLISECONDS_PER_TICK = "timeResolution";
    /**
     * <p>
     * Constant for the string describing the intended simulation start time.
     * </p>
     * 
     * @see SimulationTimer
     */
    private static final String REAL_START_TIME = "startOfSimulation";
    /**
     * <p>
     * Constant for the string describing the minimum delay until simulation start.
     * </p>
     * 
     * @see SimulationTimer
     */
    private static final String SIMULATION_START_MIN_DELAY = "minDelay";
    /**
     * <p>
     * Constant for the string describing the simulated start time.
     * </p>
     * 
     * @see SimulationTimer
     */
    private static final String SIMULATED_START_TIME = "simulatedStartTime";

    /**
     * <p>
     * Constant for the string activating or deactivating the progress GUI.
     * </p>
     */
    private static final String GUI = "gui";

    /**
     * <p>
     * Constant for the string activating or deactivating application logging.
     * </p>
     */
    private static final String LOGGING = "logging";

    /**
     * <p>
     * Constant for the string describing the simulated end time.
     * </p>
     * 
     * @see SimulationTimer
     */
    private static final String SIMULATED_END_TIME = "simulatedEndingTime";
    /**
     * <p>
     * Constant for the string describing the simulation duration.
     * </p>
     * 
     * @see SimulationTimer
     */
    private static final String SIMULATION_DURATION = "simulationTime";
    /**
     * <p>
     * Constant for the string describing the amount of threads to use during simulation.
     * </p>
     */
    private static final String EMULATION_THREAD_POOL_SIZE = "threads";

    /**
     * <p>
     * Logger for objects of this class. Configure it using <tt>/src/main/resource/log4j.xml</tt>.
     * </p>
     */
    private static final Logger LOGGER;
    /**
     * <p>
     * The timer used as a heart beat for the simulation and to either speed the simulation up or slow it down.
     * </p>
     */
    private SimulationTimer simulationTimer;

    // Catches weird bug during logger initialization when debugging.
    static {
        try {
            LOGGER = LogManager.getLogger(DataPlayer.class);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * <p>
     * Hook method for creating the factory that builds the {@link ClientEmulator}s this {@link DataPlayer} should use.
     * </p>
     * 
     * @param properties The properties provided to this {@link DataPlayer}.
     * @return An {@link EmulatorFactory} capable of constructing the correct {@link ClientEmulator} instance for this
     *         {@link DataPlayer} run.
     */
    protected abstract EmulatorFactory createFactory(final Properties properties);

    /**
     * <p>
     * Initializes the GUI for this {@link DataPlayer} if the {@code gui} property is set to {@code true}.
     * </p>
     * 
     * @param properties The properties provided to this {@link DataPlayer}, containing the {@code gui} property.
     */
    private void initGUI(final Properties properties) {
        if (Boolean.valueOf(properties.getProperty(GUI, "false"))) {
            MainWindow mainWindow = MainWindow.getMainWindow();
            simulationTimer.addTimerListener(mainWindow);
            mainWindow.setSize(600, 70);
            mainWindow.setVisible(true);

            LOGGER.info("GUI is enabled");
        } else {
            LOGGER.info("GUI is disabled: ");
        }
    }

    /**
     * <p>
     * Start this {@link DataPlayer} based on configuration from some property input (i.e. a configuration file). For
     * the format of the properties see the {@link Properties} class description. Supported properties are described by
     * the individual {@link DataPlayer} subclasses.
     * </p>
     *
     * @param propertiesInputStream The configuration data in the form of an {@link InputStream}.
     * @see Properties
     */
    public void run(final InputStream propertiesInputStream) {
        run(new InputStreamReader(propertiesInputStream));
    }

    public void run(final InputStream propertiesInputStream, final DataPlayerListener listener) {
        run(new InputStreamReader(propertiesInputStream), listener);
    }

    /**
     * <p>
     * Starts the {@code DataPlayer} based on configuration from some property input (i.e. a configuration file). For
     * the format of the properties see the {@link Properties} class description. Supported properties are described by
     * the individual {@link DataPlayer} subclasses.
     * </p>
     *
     * @param propertiesReader The reader for the configuration provided to this data player.
     * @see Properties
     */
    public void run(final Reader propertiesReader) {
        run(propertiesReader, new DataPlayerListener() {

            @Override
            public void playerFinished() {
                // Do nothing in default listener.
            }
        });
    }

    public void run(final Reader propertiesReader, final DataPlayerListener listener) {

        Properties properties = new Properties();
        try {
            properties.load(propertiesReader);

            // // calc endOfSimTime
            // simulationFactory.put(LoadConfigHelper.ENDTIME, InitHelper.calcEndOfSimeTime(simulationFactory));

            // setup simulation timer
            SimpleDateFormat dataParserWithMillis = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
            SimpleDateFormat dateParserWithSeconds = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            long simulatedStartTime = dataParserWithMillis.parse(properties.getProperty(SIMULATED_START_TIME))
                    .getTime();
            long simulatedEndTime = dataParserWithMillis.parse(properties.getProperty(SIMULATED_END_TIME)).getTime();
            String realStartTimeProperty = properties.getProperty(REAL_START_TIME);
            long realStartTime = realStartTimeProperty == null ? Calendar.getInstance().getTimeInMillis()
                    : dateParserWithSeconds.parse(realStartTimeProperty).getTime();

            long simulationInterval = Long.valueOf(properties.getProperty(SIMULATION_INTERVAL));

            long simulatedMillisecondsPerTick = Long.valueOf(properties.getProperty(SIMULATED_MILLISECONDS_PER_TICK));
            long simulationDuration = Long.valueOf(properties.getProperty(SIMULATION_DURATION, "0"));

            simulationTimer = new SimulationTimer(simulatedStartTime, simulatedEndTime, simulationDuration,
                    realStartTime, simulationInterval, simulatedMillisecondsPerTick);
            // init gui
            initGUI(properties);

            // Setup of emulation sender clients
            EmulationController emulationController = new EmulationController(Integer.valueOf(properties.getProperty(
                    EMULATION_THREAD_POOL_SIZE, String.valueOf(Runtime.getRuntime().availableProcessors()))),
                    Boolean.valueOf(properties.getProperty(LOGGING, "false")) ? Logging.ACTIVE : Logging.INACTIVE);
            simulationTimer.addTimerListener(emulationController);
            emulationController.deploy(createFactory(properties));
            simulationTimer.addTimerListener(new TimerListener() {

                @Override
                public void tick(final long tickTime) {
                    // Do nothing
                }

                @Override
                public void timerFinished() {
                    listener.playerFinished();
                }

                @Override
                public void timerStarted(final SimulationTimer timer) {
                    // Do nothing
                }
            });
            simulationTimer.start(Long.valueOf(properties.getProperty(SIMULATION_START_MIN_DELAY, "0")));
        } catch (IOException | ParseException e) {
            throw new IllegalArgumentException("Unable to load data player properties.", e);
        }
    }
}