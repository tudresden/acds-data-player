/*
 * Created at 09:07:55 on 01.09.2014
 */
package de.tudresden.inf.rn.zeebus.node;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public interface Node {
    void deploy(ScenarioPart scenarioPart);

    void play(long delay);

    void pause();

    void stop();

    void addNodeListener(NodeListener listener);
}
