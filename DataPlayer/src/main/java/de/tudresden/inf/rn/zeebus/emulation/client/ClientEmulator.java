/*
 * Created at 14:27:16 on 02.09.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.client;

import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueueListener;
import de.tudresden.inf.rn.zeebus.emulation.events.SensorEvent;
import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public interface ClientEmulator<S extends SensorEvent<?>> extends EventQueueListener, Runnable {

    void setCurrentSimulationTime(long simulationTime);

    /**
     * @return If the {@link EventQueue} of the {@code ClientEmulator} is empty.
     */
    boolean isQueueEmpty();

    /**
     * <p>
     * Needs to be called upon shutting down the associated {@link AbstractClientEmulator} and free all resources
     * associated with it. E.g. deleting an XMPP Account that was just created for the test.
     * </p>
     */
    void shutdown();

    /**
     * <p>
     * Adds the provided listener as a listener to this {@link AbstractClientEmulator}.
     * </p>
     * 
     * @param listener The listener to add.
     */
    void addClientEmulatorListener(ClientEmulatorListener listener);

    /**
     * @return The identifier for the client of this {@link ProtocolImplementation}.
     */
    String getIdentifier();

    /**
     * <p>
     * Sets a logger for this implementation, overwriting the existing logger if one exists.
     * </p>
     * 
     * @param logger The new logger for this implementation.
     */
    void setLogger(ZeebusLogger logger);

    /**
     * @return A copy of the {@link EventQueue} of this {@code ClientEmulator}. You need to use the
     *         {@code ClientEmulator}s methods to modify its {@link EventQueue} instead of using the reference returned
     *         by this method.
     */
    EventQueue<S> getEventQueue();

    /**
     * <p>
     * Connects the protocol implementation with the corresponding server. This method must be called exactly once prior
     * to using any other method from {@code ProtocolImplementation}.
     * </p>
     */
    void connect();

    void reduceEventQueue(final long simulationStartTime, final long simulationEndTime);

}