package de.tudresden.inf.rn.zeebus.logging;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ZeebusLogger {

    private final FileOutputStream fWriter;

    public ZeebusLogger(final String logFilePath, final String headerLine) {
        File targetFile = new File(logFilePath);
        File parent = targetFile.getParentFile();
        if (!parent.exists() && !parent.mkdirs()) {
            throw new IllegalStateException("Couldn't create dir: " + parent);
        }
        try {
            fWriter = new FileOutputStream(targetFile);
            fWriter.write((headerLine + "\n").getBytes("UTF-8"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void logDiscovery(final int numberMucs, final long tsbd, final long tsr, final long tsrinfo, final long dtr,
            final long dtri, final long dt) {
        StringBuilder sb = new StringBuilder();
        sb.append(numberMucs).append(";").append(tsbd).append(";").append(tsr).append(";").append(tsrinfo).append(";")
                .append(dtr).append(";").append(dtri).append(";").append(dt).append("\n");

        try {
            fWriter.write(sb.toString().getBytes("UTF-8"));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void logLocal(final long latency, final String rawMessage, final int pldSize, final int msgSize) {

        StringBuilder sb = new StringBuilder();
        sb.append(latency).append(";").append(rawMessage).append(";").append(msgSize).append(";").append(pldSize)
                .append("\n");
        try {
            fWriter.write(sb.toString().getBytes("UTF-8"));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void logState(final String msgId, final long timeSent, final String fromJid, final String toJid,
            final String rawMessage, final int pldSize, final int msgSize) {

        StringBuilder sb = new StringBuilder();
        sb.append(msgId).append(";").append(timeSent).append(";").append(fromJid).append(";").append(toJid).append(";")
                .append(rawMessage).append(";").append(msgSize).append(";").append(pldSize).append("\n");
        try {
            fWriter.write(sb.toString().getBytes("UTF-8"));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void shutDown() {
        try {
            fWriter.flush();
            fWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
