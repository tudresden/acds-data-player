/*
 * Created at 18:03:38 on 21.08.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.user;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.tudresden.inf.rn.zeebus.emulation.client.Logging;
import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class AbstractReceiverBuilder implements ReceiverBuilder {

    private static final Logger LOGGER = LogManager.getLogger(AbstractReceiverBuilder.class);

    private final String testName;
    private final ExecutorService buildExecutors;
    private final String senderHost;
    private final String receiverHost;
    private final int receiversPerSender;

    public AbstractReceiverBuilder(final String testName, final int cntBuildThreads, final String senderHost,
                                   final String receiverHost, final int receiversPerSender) {
        this.testName = testName;
        this.buildExecutors = Executors.newFixedThreadPool(cntBuildThreads);
        this.senderHost = senderHost;
        this.receiverHost = receiverHost;
        this.receiversPerSender = receiversPerSender;
    }

    /**
     * @return the senderHost
     */
    protected String getSenderHost() {
        return senderHost;
    }

    /**
     * @return the receiverHost
     */
    protected String getReceiverHost() {
        return receiverHost;
    }

    /**
     * @return the receiversPerSender
     */
    protected int getReceiversPerSender() {
        return receiversPerSender;
    }

    /**
     * @return the testName
     */
    protected String getTestName() {
        return testName;
    }

    protected Future<UserEmulator> submitToThreadPool(final UserEmulator receiver) {
        return buildExecutors.submit(receiver);
    }

    // protected ZeebusLogger getLogger() {
    // return new ZeebusLogger("logs/" + testName + "/discovery/disco-" + testName + ".csv",
    // "nmuc;tsbd;tsr;tsrinfo;dtr;dtri;dt");
    // }

    @Override
    public void shutdown() {
        buildExecutors.shutdown();
    }

    @Override
    public List<Future<UserEmulator>> createReceivers(final List<String> topics, final Logging logging) {
        List<Future<UserEmulator>> ret = new ArrayList<>();

        for (String topic : topics) {
            for (int i = 0; i < getReceiversPerSender(); i++) {
                LOGGER.info("Creating receiver #{}/{}, listening to topic {}", (ret.size() + 1), topics.size()
                        * getReceiversPerSender(), topic);
                UserEmulator receiver = internalCreateReceiver(topic, i, logging);

                ret.add(submitToThreadPool(receiver));
            }
        }

        return ret;
    }

    protected abstract UserEmulator internalCreateReceiver(final String topic, final int i, final Logging logging);

    /**
     * @param string
     * @return
     */
    protected final ZeebusLogger createLogger(final String identifier) {
        return new ZeebusLogger("logs/" + testName + "/receiver/" + identifier + ".csv",
                "id;time;fromjid;tojid;msg;msgsize;pldsize");
    }

    protected final ZeebusLogger createDiscoLogger() {
        return new ZeebusLogger("logs/" + testName + "/discovery/" + testName + ".csv",
                "nmuc;tsbd;tsr;tsrinfo;dtr;dtri;dt");
    }
}
