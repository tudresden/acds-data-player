/*
 * Created at 13:22:19 on 07.08.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.events;


/**
 * <p>
 * A listener for monitoring the state of an {@link EventQueue}.
 * </p>
 * 
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public interface EventQueueListener {
    /**
     * <p>
     * Called if the observed {@link EventQueue} was emptied.
     * </p>
     */
    void queueEmpty();
}
