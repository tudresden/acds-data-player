/*
 * Created at 09:11:44 on 01.09.2014
 */
package de.tudresden.inf.rn.zeebus.node;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public interface NodeListener {
    void finished(Node node);
}
