/*
 * Created at 17:01:32 on 19.08.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.tudresden.inf.rn.zeebus.emulation.acds.ACDSSensorEvent;
import de.tudresden.inf.rn.zeebus.emulation.acds.AbstractAcdsEmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.MqttDiscovery.DiscoveryListener;
import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.MqttDiscovery.DiscoveryTable;
import de.tudresden.inf.rn.zeebus.emulation.client.Logging;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;
import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public final class MqttFactory extends AbstractAcdsEmulatorFactory<MqttClientEmulator> {

    private final static Logger LOGGER = LogManager.getLogger(MqttFactory.class);

    public MqttFactory(final String testName, final Collection<File> scripts, final String senderHost,
                       final String userHost, final int usersPerSender) {
        super(testName, scripts, senderHost, userHost, usersPerSender);
    }

    public MqttFactory(final String testName, final File[] scripts, final String senderHost, final String userHost,
                       final int usersPerSender) {
        this(testName, Arrays.asList(scripts), senderHost, userHost, usersPerSender);
    }

    @Override
    protected MqttClientEmulator buildEmulator(final EventQueue<ACDSSensorEvent> eventQueue,
            final String[] clientDescription) {
        String identifier = null;
        outer: for (String element : clientDescription) {
            String[] splitElem = element.split("=", 2);
            switch (splitElem[0]) {
                case "id":
                    identifier = splitElem[1];
                    break outer;
            }
        }

        MqttClientEmulator ret = new MqttClientEmulator(identifier, getTestName(), getSenderHost(), eventQueue);
        return ret;
    }

    @Override
    protected final void configureLogging(final MqttClientEmulator clientEmulator, final Logging logging) {
        if (logging.equals(Logging.ACTIVE)) {
            clientEmulator.setLogger(createLogger(clientEmulator.getIdentifier(), "sender"));
            clientEmulator.setReceiverLogger(createLogger(clientEmulator.getIdentifier(), "user"));
        }
    }

    @Override
    public void testDiscoveryTime() {
        // TODO header sieht komisch aus, sleep-Zeit fließt ein etc
        // String header = "id;time;fromjid;tojid;msg;msgsize;pldsize";
        ZeebusLogger discoLogger = createDiscoLogger();

        long timeBeforeDisco = System.currentTimeMillis();
        List<String> results = discoverTopics();
        long discoLatency = System.currentTimeMillis() - timeBeforeDisco;

        discoLogger.logDiscovery(
        // erstmal wie bei MUC, mal fragen wie header oben gemeint war
                results.size(), timeBeforeDisco, 0, timeBeforeDisco + discoLatency, 0, 0, discoLatency);

        discoLogger.shutDown();

    }

    @Override
    public List<String> discoverTopics() {
        MqttDiscovery discovery = new MqttDiscovery(getSenderHost());
        String discoTopic = getTestName() + "/discovery";

        final List<String> fTopics = new ArrayList<String>();
        // final String fTestName = testName;

        MqttDiscovery.setDiscoveryListener(new DiscoveryListener() {
            @Override
            public void onDiscover(DiscoveryTable discoveryTable, String topic) {
                if (topic.contains(getTestName()))
                    fTopics.add(topic);
            }
        });

        discovery.discoverTopic(discoTopic);

        // TODO ist Mist, über Listener die Liste zu füllen und nach fester Zeit zurückzugeben.
        // Tristan hatte in onDiscover() jedes Mal gleich einen Receiver erstellt, sollte aber irgendwie einheitlich
        // sein
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            LOGGER.error("Failed to sleep while waiting for topic discoveries");
        }

        // contains static stuff
        discovery.clearDiscoveryTables();
        discovery.shutDown();

        return fTopics;
    }

    @Override
    protected UserEmulator internalCreateUser(String topic, int i, final Logging logging) {
        String clientId = topic.replace("/", ".") + ".r." + i;
        MqttReceiver mqttReceiver = new MqttReceiver(getTestName(), getUserHost(), clientId, topic);
        if (logging.equals(Logging.ACTIVE)) {
            mqttReceiver.setLogger(createLogger(clientId, "user"));
        }
        return mqttReceiver;
    }
}
