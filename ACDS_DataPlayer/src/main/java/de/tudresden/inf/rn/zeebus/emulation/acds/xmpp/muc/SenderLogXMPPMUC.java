package de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.muc;

import org.apache.commons.lang3.Validate;
import org.jivesoftware.smack.PacketInterceptor;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;

import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

public class SenderLogXMPPMUC implements PacketInterceptor {

    private final ZeebusLogger senderLogger;
    private final String idPrefix;

    public SenderLogXMPPMUC(final String idPrefix, final ZeebusLogger logger) {
        Validate.notEmpty(idPrefix);
        Validate.notNull(logger);

        this.senderLogger = logger;
        this.idPrefix = idPrefix;
    }

    @Override
    public void interceptPacket(final Packet packet) {
        Validate.notNull(packet);

        long time = System.currentTimeMillis();
        Message groupMessage = (Message)packet;
        groupMessage.setPacketID(idPrefix + "-" + groupMessage.getPacketID());

        int payloadSize = 0;
        if (groupMessage.getBody() != null) {
            payloadSize = groupMessage.getBody().getBytes().length;
        }

        senderLogger.logState(
                groupMessage.getPacketID(), time, groupMessage.getFrom(), groupMessage.getTo(), "p", payloadSize,
                groupMessage.toXML().toString().getBytes().length);
    }
}
