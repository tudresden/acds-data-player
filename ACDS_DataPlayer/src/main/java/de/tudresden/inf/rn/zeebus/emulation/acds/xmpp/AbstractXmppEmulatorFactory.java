/*
 * Created at 15:33:59 on 04.09.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.acds.xmpp;

import java.io.File;
import java.util.Collection;

import de.tudresden.inf.rn.zeebus.emulation.acds.ACDSSensorEvent;
import de.tudresden.inf.rn.zeebus.emulation.acds.AbstractAcdsEmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.ClientEmulator;
import de.tudresden.inf.rn.zeebus.emulation.client.Logging;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class AbstractXmppEmulatorFactory<E extends ClientEmulator<ACDSSensorEvent>> extends
        AbstractAcdsEmulatorFactory<E> {

    public AbstractXmppEmulatorFactory(final String testName, final Collection<File> scripts, final String senderHost,
                                       final String userHost, final int userPerClient) {
        super(testName, scripts, senderHost, userHost, userPerClient);
    }

    @Override
    protected void configureLogging(final E clientEmulator, final Logging logging) {
        if (logging.equals(Logging.ACTIVE)) {
            clientEmulator.setLogger(createLogger(clientEmulator.getIdentifier(), "sender"));
        }
    }

}
