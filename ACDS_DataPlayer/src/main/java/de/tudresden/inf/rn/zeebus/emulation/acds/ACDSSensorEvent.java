package de.tudresden.inf.rn.zeebus.emulation.acds;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import de.tudresden.inf.rn.zeebus.emulation.events.SensorEvent;

/**
 * This class represents an ACDSense SensorEvent Message. It builds the JSON Message format
 * from the given sensortype, sensorvalue and timestamp. It also has a SimulationTime-Timestamp
 * which tells when its supposed to get fired.
 * 
 * @author Philipp Grubitzsch
 *
 */
public class ACDSSensorEvent extends SensorEvent<String> {
    String timeStamp;
    String sensorType;
    String sensorValue;
    private static final DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

    // private byte[] bin_message;

    public ACDSSensorEvent(String timeStamp, String sensorType, String sensorValue) {
        this.timeStamp = timeStamp;
        this.sensorType = sensorType;
        this.sensorValue = sensorValue;
        calculateSendTime(timeStamp);
    }

    @Override
    public String getMessage() {
        StringBuilder sb = new StringBuilder();
        /*
         * output should be:
         * {"sensorevent":
         * {
         * "type":"AMBIENT_TEMPERATURE",
         * "values":[20.31],
         * "timestamp":"2014-02-24T15:16:09+01:00"}}
         */
        sb.append("{\"sensorevent\":").append("{").append("\"type\":\"").append(sensorType).append("\",")
                .append("\"values\":[").append(sensorValue).append("],").append("\"timestamp\":\"").append(timeStamp)
                .append("\"}}");
        return sb.toString();
    }

    @Override
    protected DateFormat getDateformat() {
        return FORMATTER;
    }

}
