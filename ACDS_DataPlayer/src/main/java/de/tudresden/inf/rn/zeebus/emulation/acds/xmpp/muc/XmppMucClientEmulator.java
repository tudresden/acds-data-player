package de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.muc;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.FormField;

import de.tudresden.inf.rn.zeebus.emulation.acds.ACDSSensorEvent;
import de.tudresden.inf.rn.zeebus.emulation.acds.AbstractAcdsClientEmulator;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

/**
 * <p>
 * Realizes the client simulation using XMPP as protocol and a Multi-User-Chat (MUC) for communication purposes. Clients
 * post events into this MUC and users may get events intended for them via the MUC.
 * </p>
 * <p>
 * The classes uses the Smack framework to realize the XMPP communication.
 * </p>
 * 
 * @author Philipp Grubitzsch
 * @author Klemens Muthmann
 * @version 2.0
 * @since 1.0
 */
public final class XmppMucClientEmulator extends AbstractAcdsClientEmulator {
    /**
     * <p>
     * The LOGGER for objects of this class. Configure it using <code>src/main/resources/log4j2-test.xml</code>.
     * </p>
     */
    private static final Logger LOGGER = LogManager.getLogger(XmppMucClientEmulator.class);

    /**
     * <p>
     * The Smack XMPP connection to use by this implementation.
     * </p>
     */
    private final XMPPTCPConnection connection;
    /**
     * <p>
     * The MUC to send events to and receive from.
     * </p>
     */
    private MultiUserChat muc;
    private String descriptionField;
    private final String mucUser;
    private final String username;
    private final String host;

    public XmppMucClientEmulator(final String identifier, final String testName, final String host,
                                 final EventQueue<ACDSSensorEvent> eventQueue) {
        super(identifier, testName, eventQueue);
        Validate.notEmpty(identifier);
        Validate.notEmpty(testName);
        Validate.notEmpty(host);

        this.mucUser = identifier;
        this.username = testName + "." + identifier;
        this.host = host;

        ConnectionConfiguration cconfig = new ConnectionConfiguration(host, 5222);
        cconfig.setSecurityMode(SecurityMode.disabled);

        connection = new XMPPTCPConnection(cconfig);
    }

    @Override
    public void setLogger(final ZeebusLogger logger) {
        Validate.notNull(logger);

        connection.addPacketInterceptor(new SenderLogXMPPMUC(username, logger), new MessageTypeFilter(
                Message.Type.groupchat));
    }

    private void createXMPPAccount() {
        AccountManager manager = AccountManager.getInstance(connection);
        try {
            LOGGER.info("Create XMPP Account {}", username);
            manager.createAccount(username, username);
        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            LOGGER.debug(e);
            LOGGER.warn("Account >{}< was not created: ", username);
        }

    }

    @Override
    protected void sendMessage(ACDSSensorEvent event) {
        try {
            LOGGER.trace("Send Message: {}", event.getMessage());
            muc.sendMessage(event.getMessage());
        } catch (XMPPException | NotConnectedException e) {
            throw new IllegalStateException(e);
        }
        muc.nextMessage();
    }

    public void setXmppMucDescriptionField(final String value) {
        this.descriptionField = value;
    }

    private MultiUserChat configureMuc() throws XMPPException, NoResponseException, NotConnectedException {
        muc.join(mucUser);
        // muc.changeSubject("SensorMUC");

        Form oldForm = muc.getConfigurationForm();
        Form newForm = oldForm.createAnswerForm();

        for (FormField field : oldForm.getFields()) {
            // for (Iterator<FormField> fields = oldForm.getFields(); fields.hasNext();) {
            // FormField field = (FormField) fields.next();
            if (!FormField.TYPE_HIDDEN.equals(field.getType()) && field.getVariable() != null) {
                newForm.setDefaultAnswer(field.getVariable());
            }
        }
        newForm.setTitle(oldForm.getTitle());
        newForm.setAnswer("muc#roomconfig_roomname", oldForm.getField("muc#roomconfig_roomname").getValues().get(0));
        newForm.setAnswer("muc#roomconfig_publicroom", true);
        if (descriptionField != null) {
            newForm.setAnswer("muc#roomconfig_roomdesc", descriptionField);
        }
        ArrayList<String> al = new ArrayList<String>();
        al.add("moderators");
        newForm.setAnswer("muc#roomconfig_whois", al);
        if (oldForm.getField("muc#roomconfig_historylength") != null) {
            newForm.setAnswer("muc#roomconfig_historylength", oldForm.getField("muc#roomconfig_historylength")
                    .getValues().get(0));
        }
        if (oldForm.getField("muc#roomconfig_persistentroom") != null) {
            newForm.setAnswer("muc#roomconfig_persistentroom", false);
        }
        muc.sendConfigurationForm(newForm);

        return muc;
    }

    @Override
    public void shutdown() {
        try {
            muc.leave();
            // muc.destroy("simulation finished", null);
            AccountManager.getInstance(connection).deleteAccount();
            connection.disconnect();
        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            throw new IllegalStateException(e);
        }
        super.shutdown();
    }

    @Override
    public String toString() {
        return connection.getUser().split("@")[0];
    }

    @Override
    public void connect() {
        try {
            connection.connect();
            // EntityCapsManager.getInstanceFor(connection).enableEntityCaps();
        } catch (XMPPException | SmackException | IOException e) {
            throw new IllegalStateException(e);
        }
        createXMPPAccount();
        try {
            muc = new MultiUserChat(connection, username + "@conference." + host);
        } catch (IllegalStateException e) {
            throw new IllegalStateException("Could not create MUC by user " + username, e);
        }

        try {
            connection.login(username, username, "sender");
        } catch (XMPPException | SmackException | IOException e) {
            throw new IllegalStateException("Could not login user >" + username + "< : ", e);
        }
        try {
            configureMuc();
        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            throw new IllegalStateException("Could not create MUC >" + muc.getRoom() + "< : ", e);
        }
    }

}
