package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logging;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public interface ConnectionListener {

    void setContext(Object ctx);

    void onConnecting(String clientId);

    void onConnected(String clientId);

    void onPublish(String string, MqttMessage createTopicMsg);

    void onMessageArrived(String topic, MqttMessage message);

    void onDeliveryComplete(IMqttDeliveryToken token);

    void onConnectionLost(Throwable cause);

    void onDisconnecting(String clientId);

    void onDisconnected(String clientId);

    void onError(String string, Exception e);

    void onError(String string);

    void onError(Exception e1);

    void onSubscribe(String receiverTopic);

}
