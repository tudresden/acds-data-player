package de.tudresden.inf.rn.zeebus.emulation.acds;

import java.util.EventListener;

public interface ReceiverBuiltListener extends EventListener {
    void receiverBuilt(ReceiverBuiltEvent e);
}
