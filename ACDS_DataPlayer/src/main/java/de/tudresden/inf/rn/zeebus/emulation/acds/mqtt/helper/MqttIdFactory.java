package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.helper;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;

/**
 * 
 * @author heinig
 * 
 */
public class MqttIdFactory {

  //private static int                      mid    = 1;
  private static int                      tid    = 1;
  private static int                      cid    = 1;
  private static HashMap<String, Integer> tidMap = new HashMap<String, Integer>();
  //private static HashMap<byte[], Integer> midMap = new HashMap<byte[], Integer>();

  public static int MID(byte[] payload) {
    return new String(payload).hashCode();
    // if (midMap.containsKey(payload))
    // return midMap.get(payload);
    // else
    // midMap.put(payload, mid);
    // return mid++;
  }

  public static int MID(String json) {
    return json.hashCode();
  }

  public static int TID(String topic) {
    if (tidMap.containsKey(topic))
      return tidMap.get(topic);
    else
      tidMap.put(topic, tid);
    return tid++;
  }

  public static String CID() {
    String sysUser = System.getProperty("user.name");
    String sysHost;
    try {
      sysHost = InetAddress.getLocalHost().getHostName();
    } catch (UnknownHostException e) {
      sysHost = "unknown host";
    }
    return sysUser + "@" + sysHost + cid++;
  }
}
