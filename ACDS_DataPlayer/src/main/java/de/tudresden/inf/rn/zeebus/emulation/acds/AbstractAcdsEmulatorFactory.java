/*
 * Created at 17:02:41 on 04.09.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.acds;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.AbstractXmppEmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.AbstractEmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.ClientEmulator;
import de.tudresden.inf.rn.zeebus.emulation.client.Logging;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.emulation.server.ServerEmulator;
import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;
import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class AbstractAcdsEmulatorFactory<E extends ClientEmulator<ACDSSensorEvent>> extends
        AbstractEmulatorFactory {
    private final static Logger LOGGER = LogManager.getLogger(AbstractXmppEmulatorFactory.class);
    private final static DateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

    private final Collection<File> scripts;
    private final String senderHost;
    private final String userHost;
    private final int userPerClient;
    private String userLogPath;

    public AbstractAcdsEmulatorFactory(final String testName, final Collection<File> scripts, final String senderHost,
            final String userHost, final int userPerClient) {
        super(testName);
        Validate.notEmpty(scripts);
        Validate.notEmpty(senderHost);
        Validate.notEmpty(userHost);
        Validate.isTrue(userPerClient >= 0);

        this.scripts = Collections.unmodifiableCollection(scripts);
        this.senderHost = senderHost;
        this.userHost = userHost;
        this.userPerClient = userPerClient;
        this.userLogPath = "receiver";
    }

    public AbstractAcdsEmulatorFactory(String testName, File[] scripts, String senderHost, String userHost,
            int userPerClient) {
        this(testName, Arrays.asList(scripts), senderHost, userHost, userPerClient);
    }

    public final void setUserLogPath(final String userLogPath) {
        Validate.notEmpty(userLogPath);

        this.userLogPath = userLogPath;
    }

    protected final String getUserLogPath() {
        return userLogPath;
    }

    @Override
    public final Collection<E> createClients(final Logging logging) {
        Validate.notNull(logging);

        Collection<E> ret = new HashSet<>();

        List<Future<E>> futures = new ArrayList<>();
        ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);
        try {
            for (final File script : scripts) {
                futures.add(threadPool.submit(new Callable<E>() {

                    @Override
                    public E call() throws Exception {
                        BufferedCSVInput input = new BufferedCSVInput(script);
                        EventQueue<ACDSSensorEvent> eventQueue = new EventQueue<>();
                        try {
                            String[] segs = input.getObjectLines().split(";");
                            String[] header = input.getHeaderLine().split(";");
                            String line = input.getNextLine();
                            while (line != null) {
                                Pair<Long, List<ACDSSensorEvent>> eventsOnLine = createEventsForLine(line, header);
                                eventQueue.put(eventsOnLine.getKey(), eventsOnLine.getValue());
                                line = input.getNextLine();
                            }

                            return create(eventQueue, segs, logging);
                        } finally {
                            input.close();
                        }

                    }
                }));

                for (Future<E> future : futures) {
                    ret.add(future.get());
                }
            }

        } catch (InterruptedException | ExecutionException e) {
            throw new IllegalStateException(e);
        } finally {
            threadPool.shutdown();
        }
        return ret;
    }

    protected final String getSenderHost() {
        return senderHost;
    }

    protected final String getUserHost() {
        return userHost;
    }

    private E create(final EventQueue<ACDSSensorEvent> eventQueue, final String[] clientDescription,
            final Logging logging) {
        E ret = buildEmulator(eventQueue, clientDescription);
        configureLogging(ret, logging);
        return ret;
    }

    private Pair<Long, List<ACDSSensorEvent>> createEventsForLine(final String line, final String[] header) {
        List<ACDSSensorEvent> events = new LinkedList<ACDSSensorEvent>();

        String[] s = line.endsWith(";") ? (line + " ").split(";") : line.split(";");
        String timeStamp = s[0];
        for (int i = 1; i < header.length; i++) {
            try {
                ACDSSensorEvent event = new ACDSSensorEvent(timeStamp, header[i], s[i]);
                events.add(event);
            } catch (ArrayIndexOutOfBoundsException e) {
                LOGGER.debug("Readline: {}", line);
                LOGGER.debug("i={}", i);
                LOGGER.debug("Size of s: {}", s.length);
                LOGGER.debug("Size of headerEntries: {}", header.length);
                for (int j = 0; j < header.length; j++) {
                    LOGGER.debug("Header Entry: {}", header[j]);
                }
                LOGGER.debug("Current Header Entry: {}", header[i]);
                throw e;
            }
        }
        try {
            return new ImmutablePair<Long, List<ACDSSensorEvent>>(DATE_FORMATTER.parse(timeStamp).getTime(), events);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Creates a new completely intialized {@link ClientEmulator} based on a pre filled {@link EventQueue} and a client
     * description string containing metainformation about the new {@link ClientEmulator}.
     * 
     * @param eventQueue The {@link EventQueue} for the new {@link ClientEmulator}.
     * @param clientDescription The metadata line, which is the first line of an ACDS script file.
     * @return A new {@link ClientEmulator}.
     */
    protected abstract E buildEmulator(EventQueue<ACDSSensorEvent> eventQueue, String[] clientDescription);

    protected abstract void configureLogging(E clientEmulator, Logging logging);

    @Override
    public final Collection<ServerEmulator> createServers(final Logging logging) {
        Validate.notNull(logging);
        Collection<ServerEmulator> ret = new HashSet<>();
        return ret;
    }

    @Override
    public final Collection<UserEmulator> createUsers(final Logging logging) {
        Validate.notNull(logging);
        Collection<UserEmulator> ret = new HashSet<>();
        if (userPerClient == 0) {
            return ret;
        }

        if (logging.equals(Logging.ACTIVE)) {
            testDiscoveryTime();
        }
        Collection<String> topics = discoverTopics();

        List<Future<UserEmulator>> futures = new ArrayList<>();
        ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);

        try {
            for (String topic : topics) {
                for (int i = 0; i < userPerClient; i++) {
                    LOGGER.info("Creating receiver #{}/{}, listening to topic {}", (ret.size() + 1), topics.size()
                            * userPerClient, topic);
                    UserEmulator receiver = internalCreateUser(topic, i, logging);

                    futures.add(threadPool.submit(receiver));
                }
            }

            for (Future<UserEmulator> future : futures) {
                ret.add(future.get());
            }

            return ret;
        } catch (InterruptedException | ExecutionException e) {
            throw new IllegalStateException(e);
        } finally {
            threadPool.shutdown();
        }
    }

    protected abstract void testDiscoveryTime();

    protected abstract List<String> discoverTopics();

    protected abstract UserEmulator internalCreateUser(final String topic, final int i, final Logging logging);

    protected final ZeebusLogger createDiscoLogger() {
        return new ZeebusLogger(getLogPath() + "discovery" + File.separator + getTestName() + ".csv",
                "nmuc;tsbd;tsr;tsrinfo;dtr;dtri;dt");
    }
}
