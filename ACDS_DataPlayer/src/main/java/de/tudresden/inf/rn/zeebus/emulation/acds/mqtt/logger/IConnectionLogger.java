package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logger;

import org.eclipse.paho.client.mqttv3.MqttMessage;

public interface IConnectionLogger {

  public static byte count = 0;

  public enum Event {

    UNKNOWN(-1), DISCONECTING(0), SUBSCRIBE(1), PUBLISH(2), DISCONECTED(3), SUBSCRIPTION_FAILURE(4), SUBSCRIPTION_SUCCESS(
        5), CONNECTION_LOST(6), DELIVERY_COMPLETE(7), MESSAGE_ARRIVED(8), CONNECTING(9), CONNECTED(10), TRANSPORT_READ(
        11), TRANSPORT_WRITE(12), TRANSPORT_OVERHEAD(13);
    private byte byteValue;

    Event(byte b) {
      this.byteValue = b;
    }

    Event(int b) {
      this.byteValue = (byte) b;
    }

    public static byte byteExpression(Event type) {
      return type.byteValue;
    }

    public static String getStringExpression(Event type) {
      return type.name();
    }

    public static Event valueOf(byte type) {
      for (Event e : Event.values()) {
        if (e.byteValue == type)
          return e;
      }
      return UNKNOWN;
    }

    public static Event valueOf(int type) {
      for (Event e : Event.values()) {
        if (e.byteValue == type)
          return e;
      }
      return UNKNOWN;
    }

    public String getStringExpression() {
      return name();
    }
  }

  void publish(int topicId, int messageId);

  void subscribe(int topicId);

  void disconnected();

  void subscriptionFailure(int topicId, Throwable exception);

  void subscriptionSuccess(int topicId);

  void connectionLost(Throwable cause);

  void deliveryComplete(int messageId);

  void messageArrived(int topicId, int messageId);

  void connected();

  void connecting();

  void disconnecting();

  void publish(String topic, MqttMessage m);

  void messageArrived(String topic, MqttMessage message);

}
