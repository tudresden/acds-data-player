package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CSVInputReader {

  private BufferedReader br;

  public CSVInputReader(File f) throws FileNotFoundException {
    FileReader fr = new FileReader(f);
    br = new BufferedReader(fr);
  }

  public String[] readLine(String separator) throws IOException {
    String line;
    if ((line = br.readLine()) != null)
      return line.split(separator);
    return null;
  }

  public void skipLine() throws IOException {
    br.readLine();
  }
}
