package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logger;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.helper.CSVInputReader;
import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logger.IConnectionLogger.Event;

public class LogReader {

  /**
   * @param args
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {

    CommunicationStatsRegistry registry = new LogReader().new CommunicationStatsRegistry();

    File logFile = new File("2014.03.07 13-16.csv");
    CSVInputReader reader = new CSVInputReader(logFile);
    reader.skipLine();
    String[] entries;
    while ((entries = reader.readLine(";")) != null) {
      long time = Long.parseLong(entries[0]);
      String type = entries[1];
      int nodeId = Integer.parseInt(entries[2]);
      int extra = Integer.parseInt(entries[3]);
      registry.register(time, type, nodeId, extra);
    }

    for (NodeStates ns : registry.getStats()) {
      System.out.println(ns.toString());
    }

  }

  public class CommunicationStatsRegistry {

    private HashMap<Integer, NodeStates> stats = new HashMap<Integer, NodeStates>();

    public void register(long time, String type, int nodeId, int extra) {
      getStats(nodeId).register(time, type, extra);
    }

    private NodeStates getStats(int nodeId) {
      if (stats.containsKey(nodeId))
        return stats.get(nodeId);
      NodeStates newStats = new NodeStates(nodeId);
      stats.put(nodeId, newStats);
      return newStats;
    }

    public Collection<NodeStates> getStats() {
      return stats.values();
    }
  }

  public class NodeStates {
    private HashMap<Integer, MessageStates> stats    = new HashMap<Integer, MessageStates>();
    private int                             deliveredCount;
    private float                           commonDelay;
    private long                            maxDelay = 0;
    private long                            minDelay = 99999999;
    private long                            nodeID;

    public NodeStates(int nodeId) {
      this.nodeID = nodeId;
    }

    public long getNodeID() {
      return nodeID;
    }

    public int getSentMessages() {
      return stats.size();
    }

    public int getLostMessages() {
      return getSentMessages() - getDeliveredMessages();
    }

    public int getDeliveredMessages() {
      return deliveredCount;
    }

    public float getCommonDelay() {
      return commonDelay;
    }

    public long getMaxDelay() {
      return maxDelay;
    }

    public long getMinDelay() {
      return minDelay;
    }

    public void register(long time, String type, int extra) {
      if (type.equals(Event.PUBLISH.getStringExpression()))
        getStats(extra).sent(time);
      if (type.equals(Event.MESSAGE_ARRIVED.getStringExpression()))
        getStats(extra).arrived(time);
      update();
    }

    private void update() {
      commonDelay = 0;
      deliveredCount = 0;
      for (MessageStates ms : stats.values()) {
        if (ms.delivered()) {
          deliveredCount++;
          commonDelay += ms.getDelay();
          maxDelay = ms.getDelay() > maxDelay ? ms.getDelay() : maxDelay;
          minDelay = ms.getDelay() < minDelay ? ms.getDelay() : minDelay;
        }
      }
      commonDelay = commonDelay / getDeliveredMessages();
    }

    private MessageStates getStats(int nodeId) {
      if (stats.containsKey(nodeId))
        return stats.get(nodeId);
      MessageStates newStats = new MessageStates();
      stats.put(nodeId, newStats);
      return newStats;
    }

    @Override
    public String toString() {
      return String
          .format(
              "Nodestats for Node with ID {%s}\nMessages sent {%d}, delivered {%d}, lost{%d}, common delay {%f}, min delay {%d}, max delay {%d}",
              nodeID, getSentMessages(), getDeliveredMessages(), getLostMessages(), getCommonDelay(), getMinDelay(),
              getMaxDelay());
    }
  }

  public class MessageStates {

    private long arrivedTime = 0;
    private long sentTime    = 0;

    public void sent(long time) {
      this.sentTime = time;
    }

    public void arrived(long time) {
      this.arrivedTime = time;
    }

    public long getDelay() {
      return arrivedTime - sentTime;
    }

    public boolean delivered() {
      return arrivedTime > 0 && sentTime > 0;
    }
  }
}
