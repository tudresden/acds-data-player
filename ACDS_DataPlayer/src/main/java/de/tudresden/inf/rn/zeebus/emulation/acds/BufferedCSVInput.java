package de.tudresden.inf.rn.zeebus.emulation.acds;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.IOUtils;

/**
 * Buffered read in of a given csv file. Can return headers
 * for additional information and called for the nextLine.
 * 
 * @author Philipp Grubitzsch
 *
 */
public class BufferedCSVInput {

    BufferedReader input = null;
    String objectLines;
    String headerLine;

    public BufferedCSVInput(File inputFile) {
        try {
            input = new BufferedReader(new FileReader(inputFile));
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Input file was not found " + inputFile.getName(), e);
        }
        readObjectInformation();
        readHeaderLine();
    }

    /**
     * Read out Line 3 of the CSV-like format, that contains the header information of the data
     * 
     * @return
     */
    private void readHeaderLine() {
        try {
            headerLine = input.readLine();
        } catch (IOException e) {
            throw new IllegalStateException();
        }
    }

    /**
     * Read out Line 1 of the CSV-like format, that contains Informations about the SensorObject
     * 
     * @return
     */
    private void readObjectInformation() {
        try {
            objectLines = input.readLine();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * <p>
     * Provides the next line from the underlying CSV file or {@code null} if the end of the file has been reached.
     * </p>
     * 
     * @return The next line of the underlying CSV file.
     */
    public String getNextLine() {
        try {
            return input.readLine();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public String getObjectLines() {
        return objectLines;
    }

    public String getHeaderLine() {
        return headerLine;
    }

    public void close() {
        IOUtils.closeQuietly(input);
    }

}
