/*
 * Created at 15:43:43 on 19.08.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.muc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.HostedRoom;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.RoomInfo;

import de.tudresden.inf.rn.zeebus.emulation.acds.ACDSSensorEvent;
import de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.AbstractXmppEmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.Logging;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;
import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public final class XmppMucFactory extends AbstractXmppEmulatorFactory<XmppMucClientEmulator> {

    private static final Logger LOGGER = LogManager.getLogger(XmppMucFactory.class);

    public XmppMucFactory(final String testName, final Collection<File> scripts, final String senderHost,
                          final String userHost, final int userPerClient) {
        super(testName, scripts, senderHost, userHost, userPerClient);
    }

    public XmppMucFactory(String testName, File[] loadScripts, String senderHost, String userHost, int userPerSender) {
        this(testName, Arrays.asList(loadScripts), senderHost, userHost, userPerSender);
    }

    @Override
    protected XmppMucClientEmulator buildEmulator(final EventQueue<ACDSSensorEvent> eventQueue,
            final String[] clientDescription) {
        String identifier = null;
        String longitude = null;
        String latitude = null;

        for (String seg : clientDescription) {
            String[] splitSegs = seg.split("=", 2);
            switch (splitSegs[0]) {
                case "id":
                    identifier = splitSegs[1];
                    break;
                case "long":
                    longitude = splitSegs[1];
                    break;
                case "lat":
                    latitude = splitSegs[1];
                    break;
            }
        }
        XmppMucClientEmulator clientEmulator = new XmppMucClientEmulator(identifier, getTestName(), getSenderHost(),
                eventQueue);
        clientEmulator.setXmppMucDescriptionField("{\"sensormuc\":{" + "\"type\":\"MULTI\"," + "\"format\":\"full\","
                + "\"location\":{" + "\"countryCode\":\"n.A.\"," + "\"cityName\":\"n.A.\"," + "\"latitude\":"
                + latitude + "," + "\"longitude\":" + longitude + "}}}");
        return clientEmulator;
    }

    @Override
    public void testDiscoveryTime() {
        // String filePath = "logs/" + testName + "/discovery/disco-" + testName + ".csv";
        // String headerLine = "nmuc;tsbd;tsr;tsrinfo;dtr;dtri;dt";
        ZeebusLogger loggerDisco = createDiscoLogger();

        ConnectionConfiguration cConfig = new ConnectionConfiguration(getUserHost());
        cConfig.setSecurityMode(SecurityMode.disabled);
        XMPPConnection connection = new XMPPTCPConnection(cConfig);
        String discoUser = ("discoUser" + Math.random()).replace(".", ""); // in case of >1 receivers

        try {
            // TODO EntityCapsManager.getInstanceFor(connection).enableEntityCaps();
            connection.connect();
        } catch (XMPPException | SmackException | IOException e) {
            LOGGER.debug(e);
            LOGGER.error("Failed to connect to {} ({})", getUserHost(), e.getMessage());
            return;
        }

        try {
            AccountManager manager = AccountManager.getInstance(connection);
            manager.createAccount(discoUser, discoUser);
        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            throw new IllegalStateException("Failed to create account for testing discovery time", e);
        }

        try {
            connection.login(discoUser, discoUser, getTestName());
        } catch (XMPPException | SmackException | IOException e) {
            throw new IllegalStateException("Failed to login with " + discoUser, e);
        }

        String service;
        Collection<HostedRoom> rooms;
        long timeBeforeDiscovery;
        long timeBeforeRoomInfo;
        long timeAfterDiscovery;
        long discoverLatency;
        long roomInfoDiscovery;
        long roomDiscovery;

        for (int i = 0; i < 5; i++) {

            timeBeforeDiscovery = System.currentTimeMillis();

            try {
                service = (!getSenderHost().equals(getUserHost())) ? "conference." + getSenderHost() : MultiUserChat
                        .getServiceNames(connection).iterator().next();
            } catch (XMPPException | NoResponseException | NotConnectedException e) {
                throw new IllegalStateException("Failed to get service names", e);
            }

            timeBeforeRoomInfo = System.currentTimeMillis();

            try {
                rooms = MultiUserChat.getHostedRooms(connection, service);

                for (HostedRoom room : rooms)
                    MultiUserChat.getRoomInfo(connection, room.getJid());

            } catch (XMPPException | NoResponseException | NotConnectedException e) {
                throw new IllegalStateException("Failed to get hosted rooms", e);
            }

            timeAfterDiscovery = System.currentTimeMillis();
            roomDiscovery = timeBeforeRoomInfo - timeBeforeDiscovery;
            roomInfoDiscovery = timeAfterDiscovery - timeBeforeRoomInfo;
            discoverLatency = timeAfterDiscovery - timeBeforeDiscovery;

            loggerDisco.logDiscovery(
                    rooms.size(), timeBeforeDiscovery, timeBeforeRoomInfo, timeAfterDiscovery, roomDiscovery,
                    roomInfoDiscovery, discoverLatency);
        }

        loggerDisco.shutDown();

        try {
            AccountManager.getInstance(connection).deleteAccount();
            connection.disconnect();
        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            LOGGER.debug(e);
            LOGGER.warn("Failed to delete account {}", discoUser);
        }

    }

    @Override
    public List<String> discoverTopics() {
        ConnectionConfiguration cConfig = new ConnectionConfiguration(getUserHost());
        cConfig.setSecurityMode(SecurityMode.disabled);
        XMPPConnection connection = new XMPPTCPConnection(cConfig);

        String discoUser = ("discoUser" + Math.random()).replace(".", ""); // in case of >1 receivers

        try {
            connection.connect();
        } catch (XMPPException | SmackException | IOException e) {
            LOGGER.error("Failed to connect to {} ({})", getUserHost(), e.getMessage());
            return null;
        }

        try {
            AccountManager manager = AccountManager.getInstance(connection);
            manager.createAccount(discoUser, discoUser);
        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            LOGGER.error("Failed to create account for discovery ({})", e.getMessage());
            return null;
        }

        try {
            connection.login(discoUser, discoUser, getTestName());
        } catch (XMPPException | SmackException | IOException e) {
            LOGGER.error("Failed to login with {} ({})", discoUser, e.getMessage());
            return null;
        }

        String service;
        try {
            service = (!getSenderHost().equals(getUserHost())) ? "conference." + getSenderHost() : MultiUserChat
                    .getServiceNames(connection).iterator().next();
            LOGGER.info("Looking for MUCs on: {}", service);
        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            LOGGER.error("Failed to get service name for discovery ({})", e.getMessage());
            return null;
        }

        Collection<HostedRoom> allRooms;
        List<String> relevantRooms = new ArrayList<String>();
        try {
            allRooms = MultiUserChat.getHostedRooms(connection, service);
            LOGGER.info("Found {} rooms", allRooms.size());

            for (HostedRoom room : allRooms) {
                RoomInfo roomInfo = MultiUserChat.getRoomInfo(connection, room.getJid());
                if ((roomInfo.getSubject().equals("SensorMUC")
                        || roomInfo.getDescription().contains("{\"sensormuc\":{")
                        || roomInfo.getDescription().contains("KIWI_EM_MUC") || roomInfo.getDescription().contains(
                        getTestName()))
                        && room.getName().startsWith(getTestName().toLowerCase())) {
                    relevantRooms.add(room.getJid());
                }
            }
        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            LOGGER.debug(e);
            LOGGER.error("Failed to get hosted rooms ({})", e.getMessage());
            return null;
        }

        try {
            AccountManager.getInstance(connection).deleteAccount();
            connection.disconnect();
        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            LOGGER.debug(e);
            LOGGER.warn("Failed to delete account {} ({})", discoUser, e.getMessage());
        }

        return relevantRooms;
    }

    @Override
    protected UserEmulator internalCreateUser(final String roomJid, final int index, final Logging logging) {
        String xmppResource = "rcv." + roomJid.split("@")[0] + "#" + index;
        XmppMucUserEmulator ret = new XmppMucUserEmulator(getTestName(), getUserHost(), getTestName() + ".receiver",
                "receiver", xmppResource, roomJid);
        if (logging.equals(Logging.ACTIVE)) {
            ret.setLogger(createLogger(xmppResource + "-" + roomJid.split("@")[0], getUserLogPath()));
        }
        return ret;
    }

}
