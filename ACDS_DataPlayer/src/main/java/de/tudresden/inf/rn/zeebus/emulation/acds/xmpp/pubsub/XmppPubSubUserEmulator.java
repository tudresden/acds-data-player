package de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.pubsub;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.pubsub.Item;
import org.jivesoftware.smackx.pubsub.ItemPublishEvent;
import org.jivesoftware.smackx.pubsub.LeafNode;
import org.jivesoftware.smackx.pubsub.PayloadItem;
import org.jivesoftware.smackx.pubsub.PubSubManager;
import org.jivesoftware.smackx.pubsub.listener.ItemEventListener;

import de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.AbstractXmppUserEmulator;

/**
 * <p>
 * For using an XMPP based implementation of a publish-subscribe messaging system.
 * </p>
 * 
 * @author Matthias Köngeter
 * @author Klemens Muthmann
 * @version 1.0
 * @since 1.0
 */
public class XmppPubSubUserEmulator extends AbstractXmppUserEmulator {
    private final static Logger LOGGER = LogManager.getLogger(XmppPubSubUserEmulator.class);

    private String topic;

    public XmppPubSubUserEmulator(String testName, String host, String xmppAccount, String xmppPassword,
                              String xmppResource, String topic) {
        super(testName, host, xmppAccount, xmppPassword, xmppResource);

        this.topic = topic;
        this.mRunMode = Mode.INIT;
    }

    @Override
    protected void init() {
        connect();
        login();

        try {
            PubSubManager mgr = new PubSubManager(mConnection);
            LeafNode leafNode = mgr.getNode(topic);

            leafNode.addItemEventListener(new ItemEventListener<Item>() {

                @Override
                public void handlePublishedItems(ItemPublishEvent<Item> event) {
                    if (getLogger() != null) {
                        for (Item item : event.getItems()) {
                            if (item instanceof PayloadItem<?>) {

                                PayloadItem<?> pItem = (PayloadItem<?>)item;
                                getLogger().logState(
                                        pItem.getId(), System.currentTimeMillis(), event.getNodeId(), mXmppAccount,
                                        /* pItem.toXML(), */"p",
                                        pItem.getPayload().toXML().toString().getBytes().length,
                                        pItem.toXML().getBytes().length);
                            }
                        }
                    }
                }
            });

            try {
                leafNode.subscribe(mConnection.getUser());

            } catch (XMPPException | NoResponseException | NotConnectedException e) {
                LOGGER.error("Failed to subscribe for pubsub topic {} ({})", topic, e.getMessage());
                return;
            }
        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            LOGGER.error("Failed to get pubsub node for topic {} ({})", topic, e.getMessage());
            return;
        }
    }

    @Override
    protected void shutdown() {
        disconnect();
        super.shutdown();
    }
}
