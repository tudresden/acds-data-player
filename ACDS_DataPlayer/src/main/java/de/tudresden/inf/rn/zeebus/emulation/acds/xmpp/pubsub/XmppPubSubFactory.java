/*
 * Created at 16:54:38 on 19.08.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.pubsub;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;
import org.jivesoftware.smackx.pubsub.PubSubManager;

import de.tudresden.inf.rn.zeebus.emulation.acds.ACDSSensorEvent;
import de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.AbstractXmppEmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.Logging;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;
import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public final class XmppPubSubFactory extends AbstractXmppEmulatorFactory<XmppPubSubClientEmulator> {

    private final static Logger LOGGER = LogManager.getLogger(XmppPubSubFactory.class);

    public XmppPubSubFactory(final String testName, final Collection<File> scripts, final String senderHost,
                             final String userHost, final int userPerClient) {
        super(testName, scripts, senderHost, userHost, userPerClient);
    }

    public XmppPubSubFactory(String nameOfTest, File[] scripts, String senderHost, String userHost, int userPerSender) {
        this(nameOfTest, Arrays.asList(scripts), senderHost, userHost, userPerSender);
    }

    @Override
    protected XmppPubSubClientEmulator buildEmulator(final EventQueue<ACDSSensorEvent> eventQueue,
            final String[] clientDescription) {
        String identifier = null;

        outer: for (String element : clientDescription) {
            String[] splitElem = element.split("=", 2);
            switch (splitElem[0]) {
                case "id":
                    identifier = splitElem[1];
                    break outer;
            }
        }

        XmppPubSubClientEmulator ret = new XmppPubSubClientEmulator(identifier, getTestName(), getSenderHost(),
                eventQueue);
        return ret;
    }

    @Override
    protected UserEmulator internalCreateUser(final String topic, final int i, final Logging logging) {
        String xmppResource = topic.replace("/", "").replace(".", "").replace(":", "") + "#" + i;
        XmppPubSubUserEmulator ret = new XmppPubSubUserEmulator(getTestName(), getUserHost(), getTestName()
                + ".receiver", "receiver", xmppResource, topic);
        if (logging.equals(Logging.ACTIVE)) {
            ret.setLogger(createLogger(xmppResource + "-" + topic, getUserLogPath()));
        }
        return ret;
    }

    @Override
    public void testDiscoveryTime() {
        // TODO
        // String header = "npubsub;tsbd;tsr;tsrinfo;dtr;dtri;dt";
        ZeebusLogger discoLogger = createDiscoLogger();

        long timeBeforeDisco = System.currentTimeMillis();
        List<String> results = discoverTopics();
        long discoLatency = System.currentTimeMillis() - timeBeforeDisco;

        discoLogger
                .logDiscovery(results.size(), timeBeforeDisco, 0, timeBeforeDisco + discoLatency, 0, 0, discoLatency);

        discoLogger.shutDown();

    }

    @Override
    public List<String> discoverTopics() {
        ConnectionConfiguration cConfig = new ConnectionConfiguration(getUserHost());
        cConfig.setSecurityMode(SecurityMode.disabled);
        XMPPConnection connection = new XMPPTCPConnection(cConfig);
        String discoUser = ("discoUser" + Math.random()).replace(".", ""); // in case of >1 receivers

        try {
            connection.connect();
        } catch (XMPPException | SmackException | IOException e) {
            LOGGER.error("Failed to connect to {} ({})", getUserHost(), e.getMessage());
            return null;
        }

        try {
            AccountManager manager = AccountManager.getInstance(connection);
            manager.createAccount(discoUser, discoUser);
        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            LOGGER.error("Failed to create account for discovery ({})", e.getMessage());
            return null;
        }

        try {
            connection.login(discoUser, discoUser, getTestName());
        } catch (XMPPException | SmackException | IOException e) {
            LOGGER.error("Failed to login with {} ({})", discoUser, e.getMessage());
            return null;
        }

        DiscoverItems discoItems;
        try {
            discoItems = new PubSubManager(connection).discoverNodes(null);

        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            LOGGER.error("Failed to discover nodes ({})", e.getMessage());
            return null;
        }

        List<String> nodeIDs = new ArrayList<>();
        List<DiscoverItems.Item> itemList = discoItems.getItems();

        for (DiscoverItems.Item item : itemList) {
            if (item.getNode().contains(getTestName()) && !item.getNode().contains("discovery")) {
                LOGGER.info("Topic '{}' discovered", item.getNode());
                nodeIDs.add(item.getNode());
            }
        }

        try {
            AccountManager.getInstance(connection).deleteAccount();
        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            LOGGER.warn("Failed to delete account {} ({})", discoUser, e.getMessage());
        }

        try {
            connection.disconnect();
        } catch (NotConnectedException e) {
            LOGGER.warn("Failed to disconnect {} ({})", discoUser, e.getMessage());
        }
        return nodeIDs;
    }

}
