package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logging;

public class Utils {
  public static byte[] concat(byte[] A, byte[] B) {
    int aLen = A.length;
    int bLen = B.length;
    byte[] C = new byte[aLen + bLen];
    System.arraycopy(A, 0, C, 0, aLen);
    System.arraycopy(B, 0, C, aLen, bLen);
    return C;
  }

  public static String byteArrayToString(byte[] ba) {
    StringBuilder hex = new StringBuilder(ba.length * 2);
    for (byte b : ba) {
      hex.append(String.format("%02x", b));
    }
    return hex.toString();
  }
}
