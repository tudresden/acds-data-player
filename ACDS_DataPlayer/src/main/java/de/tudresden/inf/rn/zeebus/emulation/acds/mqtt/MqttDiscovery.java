package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MqttDiscovery {
    // private static String BROKER_DISCOVERY_EXPRESSION = "BROKER/DICSOVERY/#";
    private static String DISCOVERY_TOPIC = "discovery";
    private static final int DISCOVERY_QOS = 2;
    private static MqttClient discoveryClient;
    private static MqttMessage discoveryMessage = new MqttMessage();
    private static MqttConnectOptions discoveryOptions = new MqttConnectOptions();
    private static DiscoveryListener listener;
    private static HashMap<String, DiscoveryTable> discoveries = new HashMap<String, DiscoveryTable>();
    private static ExecutorService executer = Executors.newSingleThreadExecutor();

    private static final Logger LOGGER = LogManager.getLogger(MqttDiscovery.class);

    static {
        discoveryOptions.setCleanSession(true);
        discoveryMessage.setRetained(true);
        discoveryMessage.setQos(DISCOVERY_QOS);
    }

    private String discoveryClientID;

    public MqttDiscovery(String host) {
        String sysUser = System.getProperty("user.name");
        String sysHost;
        try {
            sysHost = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            sysHost = "unknown host";
            LOGGER.error("Could not find Host: {}", e.getMessage());
        }
        discoveryClientID = sysUser + " on " + sysHost;
        discoveryClientID = discoveryClientID.length() > 22 ? discoveryClientID.substring(0, 22) : discoveryClientID;

        MemoryPersistence mp = new MemoryPersistence();
        if (discoveryClient == null)
            try {
                discoveryClient = new MqttClient(host, discoveryClientID, mp);
            } catch (MqttException e) {
                LOGGER.error("Failed to create new MqttClient: {}", e.getMessage());
                return;
            }
        discoveryClient.setCallback(new MqttCallback() {

            @Override
            public void messageArrived(String discoveryTopic, MqttMessage message) throws Exception {
                String topic = new String(message.getPayload());
                if (discoveryTopic != null && discoveryTopic.trim().length() > 0)
                    if (topic != null && topic.trim().length() > 0)
                        handleDiscovery(discoveryTopic, topic);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
            }

            @Override
            public void connectionLost(Throwable cause) {
                cause.printStackTrace();
            }
        });

        connect();

    }

    private void connect() {
        if (!discoveryClient.isConnected())
            try {
                discoveryClient.connect(discoveryOptions);
            } catch (MqttException e) {
                LOGGER.error("Failed to connect MqttClient: {}", e.getMessage());
            }
    }

    private void handleDiscovery(String discoveryTopic, String topic) {
        discoveryTopic = discoveryTopic.substring(
                0, discoveryTopic.lastIndexOf(DISCOVERY_TOPIC) + DISCOVERY_TOPIC.length());
        if (discoveries.containsKey(discoveryTopic))
            discoveries.get(discoveryTopic).getDiscoveredTopics().add(topic);
        else {
            DiscoveryTable newE = new DiscoveryTable(discoveryTopic);
            newE.getDiscoveredTopics().add(topic);
            discoveries.put(discoveryTopic, newE);
        }
        log("discovered new topic {" + topic + "}");
        if (listener != null)
            listener.onDiscover(discoveries.get(discoveryTopic), topic);
    }

    /*
     * public void discoverDefault() throws MqttException {
     * // 2 for QoS, means garanteed delivery
     * discoveryClient.subscribe(BROKER_DISCOVERY_EXPRESSION, DISCOVERY_QOS);
     * }
     */

    public void discoverTopic(String string) {
        log("started discovery for {" + string + "}");
        try {
            discoveryClient.subscribe(string + "/+/#", DISCOVERY_QOS);
        } catch (MqttException e) {
            LOGGER.error("Failed to subscribe: {}", e.getMessage());
        }
    }

    public boolean registerTopic(String topic) throws MqttDiscoveryException {
        // if (topics.contains(topic))
        // return false;
        // if (topic.startsWith(BROKER_DISCOVERY_TOPIC_PREFIX)) {
        // if (topic.length() == BROKER_DISCOVERY_TOPIC_PREFIX.length())
        // throw new MqttDiscoveryException("The given topic is not valid!");
        // register(topic);
        // } else
        // register(BROKER_DISCOVERY_TOPIC_PREFIX + topic);
        return true;
    }

    // TODO cleanup: private void register(String topic) throws MqttDiscoveryException {
    // try {
    // if (!discoveryClient.isConnected())
    // reConnect();
    // discoveryMessage.setPayload(String.format("client {%s} creates topic {%s} at time {%s}",
    // discoveryClient.getClientId(), topic, df.format(new
    // Date())).getBytes());
    // discoveryClient.publish(topic, discoveryMessage);
    // } catch (MqttException e) {
    // System.err.println(e.getMessage());
    // throw new
    // MqttDiscoveryException("Could not register topic on broker! Mqtt sending error at client connetion");
    // }
    // }

    public void clearTopics(final String topicToClear, int timeToWait) throws Exception {
        log("clear topics in {" + topicToClear + "}");
        setDiscoveryListener(new DiscoveryListener() {

            @Override
            public void onDiscover(DiscoveryTable discoveryTable, String topic) {
                log("find topic to clear  {" + topic + "}");
                executer.execute(new ClearTask(discoveryClient, topic));
                executer.execute(new ClearTask(discoveryClient, discoveryTable.getDiscoveryTopic() + "/" + topic));
            }
        });
        discoveryClient.unsubscribe(topicToClear);
        discoverTopic(topicToClear);
        CountDownLatch cdl = new CountDownLatch(1);
        cdl.await(timeToWait, TimeUnit.SECONDS);
        log("end of topics clearance in {" + topicToClear + "}");
    }

    public static DiscoveryListener getDiscoveryListener() {
        return listener;
    }

    public static void setDiscoveryListener(DiscoveryListener listener) {
        MqttDiscovery.listener = listener;
    }

    public void shutDown() {
        try {
            executer.shutdown();
            executer.awaitTermination(20, TimeUnit.SECONDS);
            if (discoveryClient.isConnected())
                discoveryClient.disconnect();
            log("shutDown");
        } catch (MqttException | InterruptedException e) {
            LOGGER.error("Shutdown Error: ", e.getMessage());
        }
    }

    private static void log(String string) {
        LOGGER.info("<< Discovery >> {}", string);
    }

    public class MqttDiscoveryException extends Exception {
        private static final long serialVersionUID = 1928833656207820036L;

        public MqttDiscoveryException(String s) {
            super(s);
        }
    }

    public static class ClearTask implements Runnable {
        private MqttClient client;
        private String topic;
        private static MqttMessage clearMessage;
        static {
            clearMessage = new MqttMessage();
            clearMessage.setRetained(true);
        }

        public ClearTask(MqttClient client, String topic) {
            this.client = client;
            this.topic = topic;
        }

        @Override
        public void run() {
            try {
                log("clears {" + topic + "}");
                client.publish(topic, clearMessage);
            } catch (MqttPersistenceException e) {
                e.printStackTrace();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    public class DiscoveryTable {

        public String discovery;
        public TreeSet<String> topics = new TreeSet<String>();

        public DiscoveryTable(String topicName) {
            this.discovery = topicName;
        }

        public String getDiscoveryTopic() {
            return discovery;
        }

        public TreeSet<String> getDiscoveredTopics() {
            return topics;
        }

    }

    public interface DiscoveryListener {

        void onDiscover(DiscoveryTable discoveryTable, String topic);

    }

    public static void main(String[] args) {
        try {
            MqttDiscovery dis = new MqttDiscovery("tcp://mobsda-dev.inf.tu-dresden.de:1883");
            dis.registerTopic("Test 7");
        } catch (MqttDiscoveryException e) {
            e.printStackTrace();
        }
    }

    public void clearDiscoveryTables() {
        log("clear all discovered topics");
        setDiscoveryListener(new DiscoveryListener() {

            @Override
            public void onDiscover(DiscoveryTable discoveryTable, String topic) {
                log("find topic to clear  {" + topic + "}");

            }
        });

        for (Entry<String, DiscoveryTable> ct : discoveries.entrySet()) {
            try {
                log("start clearing table {" + ct.getValue().getDiscoveryTopic() + "}");
                discoveryClient.unsubscribe(ct.getKey());
                for (String topic : ct.getValue().topics) {
                    executer.execute(new ClearTask(discoveryClient, topic));
                    executer.execute(new ClearTask(discoveryClient, ct.getValue().getDiscoveryTopic() + "/" + topic));
                }
            } catch (MqttException e) {
                log("ERROR while clearing table {" + ct.getValue().getDiscoveryTopic() + "}");
                e.printStackTrace();
            }
        }
    }
}
