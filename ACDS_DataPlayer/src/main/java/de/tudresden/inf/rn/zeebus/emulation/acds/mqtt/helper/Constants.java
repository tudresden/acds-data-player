package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.helper;


public interface Constants {
  public static final String SENDER_NAME   = "sender-name";
  public static final String RECEIVER_NAME = "receiver-name";
  public static final String TOPIC         = "topic";
  public static final String PUBLISH_QOS   = "publish-qos";
  public static final String SUBSCRIBE_QOS = "subscribe-qos";
  public static final String RETAIN        = "retain";
  public static final String CLEAN_SESSION = "clean-session";
  public static final String BROKER_URL    = "broker-url";
  public static final String BROKER_PORT   = "broker-PORT";
  // public static final String LOGFILE_DIR = "output" + File.separator + "log"
  // + File.separator;
  public static final String LOGFILE_DIR   = "";
  
}
