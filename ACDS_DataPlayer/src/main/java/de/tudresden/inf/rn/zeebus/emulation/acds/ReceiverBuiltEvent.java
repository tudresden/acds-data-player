package de.tudresden.inf.rn.zeebus.emulation.acds;

import java.util.EventObject;

import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;

/**
 * Event get fired if a certain receiver has been built.
 * 
 * @author Philipp
 * 
 */
public class ReceiverBuiltEvent extends EventObject {

	private static final long serialVersionUID = 6916182375606093989L;
	private UserEmulator receiver;

	public ReceiverBuiltEvent(Object source, UserEmulator receiver) {
		super(source);
		this.receiver = receiver;
		// TODO Auto-generated constructor stub
	}

	public UserEmulator getReceiver() {
		return receiver;
	}
}
