/*
 * Created at 17:51:18 on 28.10.2014
 */
package de.tudresden.inf.rn.zeebus.emulation.acds;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import de.tudresden.inf.rn.zeebus.DataPlayer;
import de.tudresden.inf.rn.zeebus.ScriptsBySizeFilter;
import de.tudresden.inf.rn.zeebus.emulation.acds.amqp.AmqpFactory;
import de.tudresden.inf.rn.zeebus.emulation.acds.coap.CoapFactory;
import de.tudresden.inf.rn.zeebus.emulation.acds.dds.DdsFactory;
import de.tudresden.inf.rn.zeebus.emulation.acds.http.HttpFactory;
import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.MqttFactory;
import de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.direct.XmppDirectFactory;
import de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.muc.XmppMucFactory;
import de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.pubsub.XmppPubSubFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.EmulatorFactory;

/**
 * <p>
 * A {@link DataPlayer} for ACDSense data supporting various protocols. The required input parameters are:
 * </p>
 * <dl>
 * <dt>testName</dt>
 * <dd>A unique name for the test used to identify resources required for the test on server side as well as during
 * logging.</dd>
 * <dt>inputFolder</dt>
 * <dd>The folder containing the ACDSense data event scripts. From these scripts the {@link AcdsDataPlayer} will use as
 * many as it requires to satisfy the amount of clients. If there are too few scripts the {@link AcdsDataPlayer} will
 * fail during initialization with an {@link IllegalStateException}.</dd>
 * <dt>clients</dt>
 * <dd>The number of clients to emulate with this {@link AcdsDataPlayer}. Each client will use a different event script
 * from the <tt>inputFolder</tt>.</dd>
 * <dt>TEST_PROTOCOL = "protocol"</dt>
 * <dd>The protocol to test. Possible values are: <tt>mqtt</tt>, <tt>xmpp_direct</tt>, <tt>xmpp_muc</tt>,
 * <tt>xmpp_pubsub</tt>, <tt>amqp</tt> <tt>http</tt>, <tt>dds</tt>, <tt>coap</tt>. However only mqtt, xmpp_muc and
 * xmpp_pubsub are implemented at the moment.</dd>
 * <dt>host</dt>
 * <dd>The host name of the server to test against.</dd>
 * <dt>port</dt>
 * <dd>The port at the host the server runs at.</dd>
 * <dt>receiversPerSender</dt>
 * <dd>The amount of receivers that should be emulated per client receiving the clients emulated messages.</dd>
 * <dt>federatedScenario</dt>
 * <dd>Either {@code true} or {@code false}. If true senders and receivers are registered at different servers
 * communicating in a federated scenario. If {@code false} all clients are using the same server.</dd>
 * <dt>host2</dt>
 * <dd>The host name for the second server to use in a federated scenario.</dd>
 * </dl>
 *
 * @author Klemens Muthmann
 *
 * @version 2.0
 * @since 1.0
 */
public final class AcdsDataPlayer extends DataPlayer {

    /**
     * <p>
     * Runs the {@link AcdsDataPlayer} based on an configuration file path and name as the first paramter.
     * </p>
     *
     * @param args The first argument is the path to the configuration file.
     * @throws IOException If the configuration file could not be read.
     */
    public static void main(final String[] args) throws IOException {
        AcdsDataPlayer player = new AcdsDataPlayer();
        InputStream configurationFileInput = null;
        try {
            configurationFileInput = new FileInputStream(args[0]);
            player.run(configurationFileInput);
        } finally {
            if (configurationFileInput != null) {
                configurationFileInput.close();
            }
        }
    }

    private static final String TEST_NAME = "testName";
    private static final String INPUT_FOLDER = "inputFolder";
    private static final String CLIENT_NUMBER = "clients";
    private static final String TEST_PROTOCOL = "protocol";
    private static final String HOST = "host";
    private static final String PORT = "port";
    private static final String RECEIVERS_PER_SENDER = "receiversPerSender";
    private static final String LOGFOLDER = "logFolder";
    private static final String FEDERATED = "federatedScenario";

    private static final String FED_RECEIVER_HOST = "host2";

    @Override
    protected EmulatorFactory createFactory(final Properties properties) {
        String testName = properties.getProperty(TEST_NAME);
        File[] scripts = loadScripts(
                new File(properties.getProperty(INPUT_FOLDER)), Integer.valueOf(properties.getProperty(CLIENT_NUMBER)));
        String senderHost = properties.getProperty(HOST);
        String userHost = properties.getProperty(FED_RECEIVER_HOST);
        int userPerClient = Integer.valueOf(properties.getProperty(RECEIVERS_PER_SENDER));
        String simulationType = properties.getProperty(TEST_PROTOCOL);
        switch (simulationType) { // TODO set new factories for the new supported protocols
            case "mqtt":
                return new MqttFactory(testName, scripts, senderHost, userHost, userPerClient);
            case "xmpp_direct":
                return new XmppDirectFactory(testName, scripts, senderHost, userHost, userPerClient);
            case "xmpp_muc":
                return new XmppMucFactory(testName, scripts, senderHost, userHost, userPerClient);
            case "xmpp_pubsub":
                return new XmppPubSubFactory(testName, scripts, senderHost, userHost, userPerClient);
            case "amqp":
                return new AmqpFactory(testName, scripts, senderHost, userHost, userPerClient);
            case "http":
                return new HttpFactory(testName, scripts, senderHost, userHost, userPerClient);
            case "dds":
                return new DdsFactory(testName, scripts, senderHost, userHost, userPerClient);
            case "coap":
                return new CoapFactory(testName, scripts, senderHost, userHost, userPerClient);
            default:
                throw new IllegalStateException("Simulation type " + simulationType + " is not supported.");
        }
    }

    /**
     * <p>
     * Loads enough event script files from the input folder to satisfy the number of clients. Large scripts files are
     * prefered if there are more than clients.
     * </p>
     *
     * @param scriptsPath The complete path to the folder containing the ACDSense event script files.
     * @param clientsNumber The number of clients to load scripts for ath the script file path.
     * @return
     */
    private File[] loadScripts(final File scriptsPath, final int clientsNumber) {
        if (!scriptsPath.exists()) {
            throw new IllegalStateException("Scripts path does not exists. Please specify a valid path.");
        }

        return scriptsPath.listFiles(new ScriptsBySizeFilter(scriptsPath, clientsNumber));
    }
}
