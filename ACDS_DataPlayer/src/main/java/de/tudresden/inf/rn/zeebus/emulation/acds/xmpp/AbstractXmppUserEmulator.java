package de.tudresden.inf.rn.zeebus.emulation.acds.xmpp;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.XMPPException.XMPPErrorException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;

import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;

/**
 * Contains general functionality for XMPP, used by both MUC and PubSub receivers.
 */
public abstract class AbstractXmppUserEmulator extends UserEmulator {

    private static final Logger LOGGER = LogManager.getLogger(AbstractXmppUserEmulator.class);
    protected XMPPConnection mConnection;
    protected final String mTestName;
    protected final String mReceiverHost;
    protected final String mXmppAccount, mXmppPassword, mXmppResource;

    protected AbstractXmppUserEmulator(final String testName, final String receiversHost, final String xmppAccount,
                                   final String xmppPassword, final String xmppResource) {
        super(testName);

        mTestName = testName;
        mReceiverHost = receiversHost;
        mXmppAccount = xmppAccount;
        mXmppPassword = xmppPassword;
        mXmppResource = xmppResource;
    }

    protected void connect() {
        try {
            ConnectionConfiguration cConfig = new ConnectionConfiguration(mReceiverHost);
            cConfig.setSecurityMode(SecurityMode.disabled);
            mConnection = new XMPPTCPConnection(cConfig);
            mConnection.connect();

        } catch (XMPPException | SmackException | IOException e) {
            LOGGER.error(mXmppAccount + " failed to connect to server (" + e.getMessage() + ")");
        }
    }

    protected void login() {
        createXmppAccount(mXmppAccount, mXmppPassword);

        try {
            if (mXmppResource != null)
                mConnection.login(mXmppAccount, mXmppPassword, mXmppResource);

            else
                mConnection.login(mXmppAccount, mXmppPassword);

        } catch (XMPPException | SmackException | IOException e) {
            LOGGER.error("{} failed to login ({})", mXmppAccount, e.getMessage());
        }
    }

    protected void disconnect() {
        // TODO
        /*
         * try {
         * // uses one account with distinct ressources
         * //AccountManager.getInstance(mConnection).deleteAccount();
         * // produces "java.net.BindException: Address already in use: JVM_Bind"
         * //mConnection.disconnect();
         * } catch (NotConnectedException | ArrayIndexOutOfBoundsException e) {
         * LOGGER.error("Already disconnected. (" + e.getMessage() + ")" + mConnection.getUser());
         * }
         */
    }

    protected void createXmppAccount(String jid, String pw) {
        AccountManager accManager = AccountManager.getInstance(mConnection);
        try {
            if (!accManager.supportsAccountCreation()) {
                LOGGER.error("Automatic account creation not supported!");
            }

            else
                try {
                    accManager.createAccount(jid, pw);

                } catch (XMPPException e) {
                    LOGGER.warn("Failed to create account (" + e.getMessage() + ")");
                }

        } catch (NoResponseException | XMPPErrorException | NotConnectedException e) {
            System.out.println("[ERROR] Failed to create account (" + e.getMessage() + ")");
        }
    }

    @Override
    public String toString() {
        return mConnection.getUser();
    }
}
