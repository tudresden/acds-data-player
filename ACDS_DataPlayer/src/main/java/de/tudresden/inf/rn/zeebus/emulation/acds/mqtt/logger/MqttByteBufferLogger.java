package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.internal.wire.MqttWireMessage;

import de.tudresden.inf.rn.heinig.steamlistener.IMqttWireListener;
import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.helper.Constants;
import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.helper.EventCodec;

public class MqttByteBufferLogger<T> implements IConnectionLogger, IMqttWireListener {

  // static

  private static int                      NO_EXTRA = -1;
  private static int                      errorID  = 0;
  private static HashMap<Integer, String> errorMap = new HashMap<Integer, String>();

  // non static
  public T                                node;
  private int                             nodeId;
  private EventCodecImpl                  codec;
  public boolean                          writeLog = false;

  public MqttByteBufferLogger(T node, boolean writeLog) {
    this.node = node;
    this.nodeId = node.hashCode();
    this.codec = new EventCodecImpl();
    this.writeLog = writeLog;
  }

  private void internal(Event type, int extra) {
    codec.encode(System.currentTimeMillis(), Event.byteExpression(type), nodeId, extra);
  }

  private void internal(Event type) {
    codec.encode(System.currentTimeMillis(), Event.byteExpression(type), nodeId, NO_EXTRA);
  }

  private void internal(Event type, Throwable cause) {
    errorID++;
    errorMap.put(errorID, cause.getMessage());
    codec.encode(System.currentTimeMillis(), Event.byteExpression(type), nodeId, errorID);
  }

  @Override
  public void connected() {
    internal(Event.CONNECTED);
  }

  @Override
  public void connecting() {
    internal(Event.CONNECTING);
  }

  @Override
  public void messageArrived(int topicId, int messageId) {
    internal(Event.MESSAGE_ARRIVED, messageId);
  }

  @Override
  public void messageArrived(String topic, MqttMessage message) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void deliveryComplete(int messageId) {
    internal(Event.DELIVERY_COMPLETE, messageId);
  }

  @Override
  public void connectionLost(Throwable cause) {
    internal(Event.CONNECTION_LOST, cause);
  }

  @Override
  public void subscriptionSuccess(int topicId) {
    internal(Event.SUBSCRIPTION_SUCCESS);
  }

  @Override
  public void subscriptionFailure(int topicId, Throwable cause) {
    internal(Event.SUBSCRIPTION_FAILURE, cause);
  }

  @Override
  public void disconnected() {
    internal(Event.DISCONECTED);
  }

  @Override
  public void publish(int topicId, int messageId) {
    internal(Event.PUBLISH, messageId);
  }

  @Override
  public void publish(String topic, MqttMessage m) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void subscribe(int messageId) {
    internal(Event.SUBSCRIBE, messageId);
  }

  @Override
  public void disconnecting() {
    internal(Event.DISCONECTING);
  }

  public void debug() {
    codec.decode();
  }

  public static String getEventName(int type) {
    return Event.valueOf(type).getStringExpression();
  }

  @Override
  public void onReadMqttWireMessage(MqttWireMessage msg) {
    try {
      internal(Event.TRANSPORT_READ, msg.getHeader().length + msg.getPayload().length);
    } catch (MqttException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onWriteMqttWireMessage(MqttWireMessage msg) {
    try {
      internal(Event.TRANSPORT_WRITE, msg.getHeader().length + msg.getPayload().length);
      internal(Event.TRANSPORT_OVERHEAD, msg.getHeader().length);
    } catch (MqttException e) {
      e.printStackTrace();
    }
  }

  public class EventCodecImpl extends EventCodec {

    private OutputStreamWriter fw;
    SimpleDateFormat           sdf = new SimpleDateFormat("yyyy.MM.dd HH-mm");

    @Override
    public void decode(long time, byte type, int nodeId, int extra) {
      System.out.printf("%d - %s - %d - %d\n", time, getEventName(type), nodeId, extra);
      if (writeLog) {
        try {
          open();
          fw.write(String.format("%d;%s;%d;%d\n", time, getEventName(type), nodeId, extra));
        } catch (IOException e) {
          e.printStackTrace();
        }
      }

    }

    private void open() throws IOException {
      if (fw == null) {
        fw = new FileWriter(Constants.LOGFILE_DIR + sdf.format(new Date()) + ".csv");
        fw.write(String.format("%s;%s;%s;%s\n", "Zeitstempel", "Event", "Node-ID", "EXTRA"));
      }
    }

    @Override
    public void finish() {
      try {
        if (fw != null) {
          fw.flush();
          fw.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

}
