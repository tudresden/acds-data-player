package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logger;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logging.ConnectionListener;

public class MqttConnectionListener implements ConnectionListener {

    private String clientId;

    @Override
    public void setContext(Object ctx) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnecting(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public void onConnected(String clientId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPublish(String topic, MqttMessage message) {
        // log(String.format("%s publish T:{%s} M:{%s} ", clientId, topic,
        // message.toString()));
    }

    @Override
    public void onSubscribe(String topic) {
        log(String.format("%s subscribe T:{%s}", clientId, topic));
    }

    @Override
    public void onMessageArrived(String topic, MqttMessage message) {
        // log(String.format("%s receive T:{%s} M:{%s} ", clientId, topic,
        // message.toString()));
    }

    @Override
    public void onDeliveryComplete(IMqttDeliveryToken token) {

    }

    @Override
    public void onConnectionLost(Throwable cause) {
        error(String.format("%s lost connection", clientId), cause);
    }

    @Override
    public void onDisconnecting(String clientId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDisconnected(String clientId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onError(String string, Exception cause) {
        error(string, cause);
    }

    @Override
    public void onError(String string) {
        error(string, null);
    }

    @Override
    public void onError(Exception cause) {
        error(cause.getMessage(), cause);
    }

    private void log(String string) {
        System.out.println(string);
    }

    private void error(String text, Throwable cause) {
        System.err.println(text);
        System.err.println("Trace begin {");
        if (cause != null)
            cause.printStackTrace();
        System.err.println("} Trace end");
    }
}
