package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt;

import java.io.IOException;

import org.apache.commons.lang3.Validate;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import de.tudresden.inf.rn.zeebus.emulation.acds.ACDSSensorEvent;
import de.tudresden.inf.rn.zeebus.emulation.acds.AbstractAcdsClientEmulator;
import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.helper.SystemMonitoring;
import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logging.ConnectionListener;
import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logging.MqttACDSLogger;
import de.tudresden.inf.rn.zeebus.emulation.client.ClientEmulator;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

/**
 * <p>
 * MQTT {@link ClientEmulator} for processing {@link ACDSSensorEvent}s.
 * </p>
 * 
 * @author Tristan Heinig
 * @author Klemens Muthmann
 * @author Philipp Grubitzsch
 * @version 2.0
 * @since 1.0
 */
public class MqttClientEmulator extends AbstractAcdsClientEmulator {

    private static final int PUBLISH_QOS = 0;
    private static final boolean RETAIN = false;
    private final MqttClient client;
    private final String topic;
    // private boolean debug = true;
    private boolean sysMonitoring = false;
    private final ConnectionListener conListener;
    private final String nameOfTest;

    private ZeebusLogger receiverLogger;

    public MqttClientEmulator(final String identifier, final String nameOfTest, final String senderHost,
                              final EventQueue<ACDSSensorEvent> eventQueue) {
        super(identifier, nameOfTest, eventQueue);
        Validate.notEmpty(identifier);

        this.topic = nameOfTest + "/" + identifier;
        this.nameOfTest = nameOfTest;

        // Define how and where data is stored
        MqttClientPersistence persistence = new MemoryPersistence();

        try {
            client = new MqttClient(senderHost, nameOfTest + "." + identifier + ".s", persistence);
            // String header = "id;time;fromjid;tojid;msg;msgsize;pldsize";
            // String logpath = "logs/" + nameOfTest + "/sender/s_" + client.getClientId().toLowerCase() + ".csv";
            conListener = new MqttACDSLogger(getLogger(), receiverLogger);

            conListener.setContext(client);
        } catch (MqttException e) {
            // conListener.onError("error while trying to establish mqtt connection", e);
            throw new IllegalStateException(e);
        }
    }

    public void setReceiverLogger(final ZeebusLogger receiverLogger) {
        this.receiverLogger = receiverLogger;
    }

    /**
     * Creates a topic at the mqtt broker and publish one retained message with
     * ensured delivery, if no other retained message will be send to the topic
     * 
     * @param topic
     * @param discoveryTopic
     * @throws MqttPersistenceException
     * @throws MqttException
     */
    private void createTopic(final String topic, final String discoveryTopic) throws MqttPersistenceException,
            MqttException {
        if (client != null && client.isConnected()) {

            MqttMessage createTopicMsg = new MqttMessage(topic.getBytes());
            createTopicMsg.setQos(2);
            createTopicMsg.setRetained(true);

            conListener.onPublish(discoveryTopic + "/" + topic, createTopicMsg);
            client.publish(discoveryTopic + "/" + topic, createTopicMsg);
            client.setCallback(new MqttCallback() {

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    conListener.onMessageArrived(topic, message);
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    conListener.onDeliveryComplete(token);
                }

                @Override
                public void connectionLost(Throwable cause) {
                    conListener.onConnectionLost(cause);
                }
            });
        }
    }

    @Override
    public void sendMessage(final ACDSSensorEvent event) {

        if (sysMonitoring) {
            try {
                startSysMonitoring(new Runnable() {

                    @Override
                    public void run() {
                        if (client != null && client.isConnected()) {
                            try {
                                MqttMessage msg = new MqttMessage(event.getMessage().getBytes());
                                msg.setQos(PUBLISH_QOS);
                                msg.setRetained(RETAIN);

                                conListener.onPublish(topic, msg);
                                client.publish(topic, msg);

                            } catch (MqttPersistenceException e) {
                                conListener.onError("Intern mqtt persistence error!", e);
                            } catch (MqttException e) {
                                conListener.onError("Intern mqtt error!", e);
                            }
                        } else
                            conListener.onError("Sending error. Mqtt-client is not connected!");
                    }
                });
            } catch (IOException e1) {
                conListener.onError(e1);
            }
        } else {
            if (client != null && client.isConnected()) {
                try {
                    MqttMessage msg = new MqttMessage(event.getMessage().getBytes());
                    msg.setQos(PUBLISH_QOS);
                    msg.setRetained(RETAIN);

                    conListener.onPublish(topic, msg);
                    client.publish(topic, msg);
                } catch (MqttPersistenceException e) {
                    conListener.onError("Intern mqtt persistence error!", e);
                } catch (MqttException e) {
                    conListener.onError("Intern mqtt error!", e);
                }
            } else
                conListener.onError("Sending error. Mqtt-client is not connected!");
        }

    }

    @Override
    public void shutdown() {
        if (client != null && client.isConnected()) {
            try {
                conListener.onDisconnecting(client.getClientId());
                client.disconnect();
                conListener.onDisconnected(client.getClientId());
            } catch (MqttException e) {
                conListener.onError("error while trying to disconnect mqtt client", e);
                e.printStackTrace();
            }
        }

        if (receiverLogger != null) {
            receiverLogger.shutDown();
        }
        super.shutdown();

    }

    private void startSysMonitoring(Runnable run) throws IOException {

        SystemMonitoring.startCpuMeasuring();
        run.run();
        float cpuPercentage = SystemMonitoring.endCpuMeasuring();
        System.out.println(cpuPercentage * 100);

    }

    public ConnectionListener getConnectionListener() {
        return conListener;
    }

    @Override
    public String toString() {
        return topic;
    }

    @Override
    public void connect() {
        // Define MQTT connection options like timeout, password, ...
        MqttConnectOptions options = new MqttConnectOptions();

        conListener.onConnecting(client.getClientId());
        try {
            client.connect(options);
            conListener.onConnected(client.getClientId());

            createTopic(topic, nameOfTest + "/discovery");
        } catch (MqttException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
