package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logging;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.internal.wire.MqttInputStream;
import org.eclipse.paho.client.mqttv3.internal.wire.MqttOutputStream;
import org.eclipse.paho.client.mqttv3.internal.wire.MqttPublish;
import org.eclipse.paho.client.mqttv3.internal.wire.MqttWireMessage;

import de.tudresden.inf.rn.heinig.steamlistener.IMqttWireListener;
import de.tudresden.inf.rn.heinig.steamlistener.MyMqttInputStream;
import de.tudresden.inf.rn.heinig.steamlistener.MyMqttOutputStream;
import de.tudresden.inf.rn.zeebus.logging.ZeebusLogger;

public class MqttACDSLogger implements ConnectionListener, IMqttWireListener {

    // TODO cleanup: static
    // private static HashMap<Integer, String> errorMap = new HashMap<Integer, String>();
    // private static ExecutorService executer = Executors.newFixedThreadPool(4);

    // non static
    public MqttClient node;
    private final ZeebusLogger sendLogger;
    private final ZeebusLogger receiveLogger;
    private final HashFactory hashFactory = new HashFactory();
    public boolean readableHash = true;

    public MqttACDSLogger(final ZeebusLogger senderLogger, final ZeebusLogger receiverLogger) {
        this.receiveLogger = senderLogger;
        this.sendLogger = receiverLogger;
    }

    public MqttACDSLogger(final ZeebusLogger receiverLogger) {
        this.receiveLogger = receiverLogger;
        this.sendLogger = null;
    }

    private void internalSend(String msgId, String from, String to, MqttWireMessage m, long time) throws MqttException {
        if (sendLogger == null)
            return;
        // throw new NullPointerException(
        // "The ZeebusLogger for sending events is not initialized. Please use the corresponding constructor!");

        // sendLogger.writeMessageToLog(msgId, System.currentTimeMillis(), from,
        // to,
        // new String(Utils.concat(m.getHeader(), m.getPayload())),
        // m.getPayload().length,
        // m.getPayload().length + m.getHeader().length);
        sendLogger.logState(msgId, time, from, to, "null", m.getPayload().length, m.getPayload().length
                + m.getHeader().length);
    }

    private void internalReceive(String msgId, String from, String to, MqttWireMessage m, long time)
            throws MqttException {
        if (receiveLogger == null)
            throw new NullPointerException(
                    "The ZeebusLogger for receiving events is not initialized. Please use the corresponding constructor!");
        // receiveLogger.writeMessageToLog(msgId, System.currentTimeMillis(),
        // from,
        // to,
        // new String(Utils.concat(m.getHeader(), m.getPayload())),
        // m.getPayload().length,
        // m.getPayload().length + m.getHeader().length);
        receiveLogger.logState(
                msgId, time, from, to, "null", m.getPayload().length, m.getPayload().length + m.getHeader().length);
    }

    @Override
    public void setContext(Object ctx) {
        if (ctx instanceof MqttClient)
            node = (MqttClient)ctx;
        else
            throw new IllegalArgumentException("The given context object must be of type MqttClient");
    }

    @Override
    public void onConnecting(String clientId) {
    }

    @Override
    public void onConnected(String clientId) {
        MqttInputStream in = node.getClientComms().receiver.in;
        MqttOutputStream out = node.getClientComms().sender.out;
        ((MyMqttInputStream)in).setListener(this);
        ((MyMqttOutputStream)out).setListener(this);
        log(clientId + " established connection");
    }

    @Override
    public void onPublish(String topic, MqttMessage msg) {
        // log(node.getClientId() + " publishes in " + topic);
        // internalSend(
        // Utils.byteArrayToString(hashFactory.createHash(
        // topic, msg.getPayload())),
        // node.getClientId(), topic, msg,
        // System.currentTimeMillis());
    }

    @Override
    public void onSubscribe(String receiverTopic) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onMessageArrived(String topic, MqttMessage message) {
    }

    @Override
    public void onDeliveryComplete(IMqttDeliveryToken token) {
    }

    @Override
    public void onConnectionLost(Throwable cause) {
        error(node.getClientId() + " lost connection", cause);
    }

    @Override
    public void onDisconnecting(String clientId) {
    }

    @Override
    public void onDisconnected(String clientId) {
        log(clientId + " closed connection");
    }

    @Override
    public void onError(String string, Exception cause) {
        error(string, cause);
    }

    @Override
    public void onError(String string) {
        error(string, null);
    }

    @Override
    public void onError(Exception cause) {
        error(cause.getMessage(), cause);
    }

    @Override
    public void onReadMqttWireMessage(final MqttWireMessage msg) {
        long time = System.currentTimeMillis();
        if (msg.getType() == MqttWireMessage.MESSAGE_TYPE_PUBLISH) {
            MqttPublish pubMsg = (MqttPublish)msg;
            try {
                if (readableHash)
                    internalReceive(
                            Utils.byteArrayToString(hashFactory.createHash(pubMsg.getTopicName(), pubMsg.getPayload())),
                            pubMsg.getTopicName(), node.getClientId(), msg, time);
                else
                    internalReceive(
                            new String(hashFactory.createHash(pubMsg.getTopicName(), msg.getPayload())),
                            pubMsg.getTopicName(), node.getClientId(), msg, time);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onWriteMqttWireMessage(MqttWireMessage msg) {
        long time = System.currentTimeMillis();
        if (msg.getType() == MqttWireMessage.MESSAGE_TYPE_PUBLISH) {
            MqttPublish pubMsg = (MqttPublish)msg;
            try {
                if (readableHash)
                    internalSend(
                            Utils.byteArrayToString(hashFactory.createHash(pubMsg.getTopicName(), msg.getPayload())),
                            node.getClientId(), pubMsg.getTopicName(), msg, time);
                else
                    internalSend(
                            new String(hashFactory.createHash(pubMsg.getTopicName(), msg.getPayload())),
                            node.getClientId(), pubMsg.getTopicName(), msg, time);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    public class HashFactory {
        // private MessageDigest engine;
        private ByteBuffer buffer = ByteBuffer.allocate(2048);

        // static {
        // try {
        // HashFactory.engine = MessageDigest.getInstance("MD5");
        // } catch (NoSuchAlgorithmException e) {
        // e.printStackTrace();
        // }
        // };

        public byte[] createHash(String topic, byte[] payload) throws UnsupportedEncodingException {
            synchronized (buffer) {
                buffer.clear();
                buffer.put(topic.getBytes("UTF-8")).put(payload);
                try {
                    return MessageDigest.getInstance("MD5").digest(buffer.array());
                } catch (NoSuchAlgorithmException e) {
                    error(e.getMessage(), e);
                    return new byte[0];
                }
            }
        }

        final protected char[] hexArray = "0123456789ABCDEF".toCharArray();

        public String bytesToHex(byte[] bytes) {
            synchronized (bytes) {
                char[] hexChars = new char[bytes.length * 2];
                for (int j = 0; j < bytes.length; j++) {
                    int v = bytes[j] & 0xFF;
                    hexChars[j * 2] = hexArray[v >>> 4];
                    hexChars[j * 2 + 1] = hexArray[v & 0x0F];
                }
                return new String(hexChars);
            }
        }
    }

    private static void log(String string) {
        synchronized (System.out) {
            System.out.println(string);
        }
    }

    private static void error(String text, Throwable cause) {
        synchronized (System.err) {
            System.err.println(text);
            System.err.println("Trace begin {");
            if (cause != null)
                cause.printStackTrace();
            System.err.println("} Trace end");
        }

    }

    // TODO cleanup: public abstract class LogTask implements Runnable {
    //
    // private long logTime;
    //
    // public LogTask(long currentTimeMillis) {
    // this.logTime = currentTimeMillis;
    // }
    //
    // }
}
