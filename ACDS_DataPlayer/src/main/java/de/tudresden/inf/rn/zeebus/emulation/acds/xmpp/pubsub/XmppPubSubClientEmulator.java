package de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.pubsub;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.XMPPException.XMPPErrorException;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.pubsub.AccessModel;
import org.jivesoftware.smackx.pubsub.ConfigureForm;
import org.jivesoftware.smackx.pubsub.FormType;
import org.jivesoftware.smackx.pubsub.LeafNode;
import org.jivesoftware.smackx.pubsub.NodeType;
import org.jivesoftware.smackx.pubsub.PayloadItem;
import org.jivesoftware.smackx.pubsub.PubSubManager;
import org.jivesoftware.smackx.pubsub.PublishModel;
import org.jivesoftware.smackx.pubsub.SimplePayload;

import de.tudresden.inf.rn.zeebus.emulation.acds.ACDSSensorEvent;
import de.tudresden.inf.rn.zeebus.emulation.acds.AbstractAcdsClientEmulator;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;

/**
 * <p>
 * Protocol: XMPP / specific Implemenation: PubSub
 * </p>
 * 
 * @author Matthias Köngeter
 * @author Klemens Muthmann
 * @author Philipp Grubitzsch
 * @version 2.0
 * @since 1.0
 */
public class XmppPubSubClientEmulator extends AbstractAcdsClientEmulator {

    /**
     * <p>
     * The LOGGER used by objects of this class. Configure it using src/main/resources/log4j2-test.xml.
     * </p>
     */
    private static final Logger LOGGER = LogManager.getLogger(XmppPubSubClientEmulator.class);

    private static final String USER_RESOURCE = "server";

    /** Connection object representing connection to XMPP server */
    private XMPPConnection connection;
    /* Leaf node, advertising topics to clients */
    // private LeafNode mDiscoveryNode;
    /** Leaf node, publishing all messages belonging to the assigned topic. */
    private LeafNode topicNode;
    /** Item to be published. Declared here to minimize garbage collector / memory allocation overhead */
    private PayloadItem<PacketExtension> item;
    /** The LOGGER for writing all sent messages into a file. */
    private final String username;
    private final String topic;

    /**
     * <p>
     * Creates a new {@code XmppPubSubClientEmulator} ready to be used. Before calling any other methods you need to
     * connect to the XMPP server using {@link #connect()}.
     * </p>
     * 
     * @param identifier The identifier for the connection, used to create resources on server side.
     * @param testName The name of the test to run, which is also used as part of the name of the resources on server
     *            side.
     * @param host The host XMPP server.
     */
    public XmppPubSubClientEmulator(final String identifier, final String testName, final String host,
                                    final EventQueue<ACDSSensorEvent> eventQueue) {
        super(identifier, testName, eventQueue);
        username = testName + ".sender." + identifier;
        topic = testName + "/" + identifier;

        // connect to XMPP server
        ConnectionConfiguration cConfig = new ConnectionConfiguration(host, 5222);
        cConfig.setSecurityMode(SecurityMode.disabled);
        connection = new XMPPTCPConnection(cConfig);
    }

    /**
     * <p>
     * Create a leaf node for this sender's topic.
     * </p>
     * <p>
     * Publish the topic via the discovery node.
     * </p>
     */
    private void createTopicNode() {
        // create leaf node
        ConfigureForm nodeConf = new ConfigureForm(FormType.submit);
        nodeConf.setNodeType(NodeType.leaf);
        nodeConf.setAccessModel(AccessModel.open);
        nodeConf.setPublishModel(PublishModel.publishers);
        nodeConf.setDeliverPayloads(true);
        nodeConf.setNotifyDelete(true);
        nodeConf.setPersistentItems(false);
        nodeConf.setPresenceBasedDelivery(false); // for performance

        try {
            topicNode = (LeafNode)new PubSubManager(connection).createNode(topic, nodeConf);

            LOGGER.info("Topic node created ({})");
        } catch (NoResponseException | XMPPErrorException | NotConnectedException e) {
            LOGGER.info("failed to create topic node ({})");

            try {
                topicNode = new PubSubManager(connection).getNode(topic);
                LOGGER.info("Retrieved existing topic node {}", topicNode.getId());

            } catch (NoResponseException | XMPPErrorException | NotConnectedException e1) {
                throw new IllegalStateException("failed to retrieve existing topic node", e1);
            }
        }
    }

    @Override
    protected void sendMessage(final ACDSSensorEvent event) {
        item = new PayloadItem<PacketExtension>(new SimplePayload(topicNode.getId(), null, "<event>"
                + event.getMessage() + "</event>"));

        try {
            topicNode.publish(item);
            logEvent(item, System.currentTimeMillis());

        } catch (NotConnectedException e) {
            throw new IllegalStateException("Failed to send item", e);
        }
    }

    private void logEvent(PayloadItem<PacketExtension> item, long timestamp) {
        if (getLogger() != null) {

            getLogger().logState(
                    username + "-" + item.getId(), timestamp, username, topic, "p",
                    item.getPayload().toXML().toString().getBytes().length, item.toXML().toString().getBytes().length);
        }
    }

    @Override
    public void shutdown() {
        PubSubManager psManager = new PubSubManager(connection);

        try {
            LOGGER.info("Deleting topic node ({})", topicNode.getId());
            psManager.deleteNode(topicNode.getId());
        } catch (NoResponseException | XMPPErrorException | NotConnectedException e) {
            throw new IllegalStateException("failed to delete topic node", e);
        }

        try {
            AccountManager.getInstance(connection).deleteAccount();
            connection.disconnect();
        } catch (NotConnectedException | NoResponseException | XMPPErrorException e) {
            throw new IllegalStateException("Failed to delete account/disconnect", e);
        }
        super.shutdown();
    }

    @Override
    public String toString() {
        return connection.getUser().split("@")[0];
    }

    @Override
    public void connect() {
        try {
            connection.connect();
        } catch (SmackException | IOException | XMPPException e) {
            throw new IllegalStateException(e);
        }

        // create XMPP account
        AccountManager accManager = AccountManager.getInstance(connection);
        try {
            accManager.createAccount(username, username);
        } catch (NoResponseException | XMPPErrorException | NotConnectedException e) {
            LOGGER.warn("User {} already exists.", username);
        }

        // login to XMPP server
        try {
            connection.login(username, username, USER_RESOURCE);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to login on XMPP server", e);
        }

        // initDiscoveryNode();
        createTopicNode();
    }
}
