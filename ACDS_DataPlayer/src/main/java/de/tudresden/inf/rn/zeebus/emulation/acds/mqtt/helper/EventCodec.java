package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.helper;


import java.nio.ByteBuffer;

public abstract class EventCodec {
  ByteBuffer  buffer          = ByteBuffer.allocateDirect(12 * 1024 * 1024);
  private int bufferEnd;
  public int  bufferEntrySize = 8 + 1 + 4 + 4;

  public abstract void decode(long time, byte type, int nodeId, int extra);

  public void encode(long time, byte type, int nodeId, int extra) {
    synchronized (buffer) {
      buffer.putLong(time);
      buffer.put(type);
      buffer.putInt(nodeId);
      buffer.putInt(extra);
    }
    bufferEnd = buffer.position();
  }

  public void decode() {
    buffer.rewind();
    while (buffer.position() < bufferEnd) {
      long time = buffer.getLong();
      byte type = buffer.get();
      int nodeId = buffer.getInt();
      int extra = buffer.getInt();
      decode(time, type, nodeId, extra);
    }
    finish();
  }

  abstract public void finish();
}