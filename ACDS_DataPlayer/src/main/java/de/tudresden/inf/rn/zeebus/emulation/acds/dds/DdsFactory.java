package de.tudresden.inf.rn.zeebus.emulation.acds.dds;

import java.io.File;
import java.util.List;

import de.tudresden.inf.rn.zeebus.emulation.acds.ACDSSensorEvent;
import de.tudresden.inf.rn.zeebus.emulation.acds.AbstractAcdsEmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.Logging;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;

public class DdsFactory extends AbstractAcdsEmulatorFactory<DdsClientEmulator> {


    public DdsFactory(String testName, File[] scripts, String senderHost, String userHost, int userPerClient) {
        super(testName, scripts, senderHost, userHost, userPerClient);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected DdsClientEmulator buildEmulator(EventQueue<ACDSSensorEvent> eventQueue, String[] clientDescription) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void configureLogging(DdsClientEmulator clientEmulator, Logging logging) {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected void testDiscoveryTime() {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected List<String> discoverTopics() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected UserEmulator internalCreateUser(String topic, int i, Logging logging) {
        // TODO Auto-generated method stub
        return null;
    }

}
