package de.tudresden.inf.rn.zeebus.emulation.acds.mqtt;

import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logging.ConnectionListener;
import de.tudresden.inf.rn.zeebus.emulation.acds.mqtt.logging.MqttACDSLogger;
import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;

public class MqttReceiver extends UserEmulator {

    private static final Logger LOGGER = LogManager.getLogger(MqttReceiver.class);

    private final String mHost;
    private final String mTopic;
    private final String mClientId;
    private MqttClient mMqttClient;
    private MqttConnectOptions mMqttOptions;
    private ConnectionListener mLogger;

    public MqttReceiver(final String testName, final String host, final String clientId, final String topic) {
        super(testName);
        Validate.notEmpty(host);
        Validate.notEmpty(clientId);
        Validate.notEmpty(topic);

        mHost = host;
        mTopic = topic;
        mClientId = clientId;

        mRunMode = Mode.INIT;
    }

    @Override
    protected void init() {
        mMqttOptions = new MqttConnectOptions();
        mMqttOptions.setCleanSession(true);
        MqttClientPersistence persistence = new MemoryPersistence();

        try {
            mMqttClient = new MqttClient(mHost, mClientId, persistence);
            mMqttClient.setCallback(new MqttCallback() {

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    mLogger.onMessageArrived(topic, message);
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                }

                @Override
                public void connectionLost(Throwable cause) {
                    mLogger.onConnectionLost(cause);
                }
            });

            // String header = "id;time;fromjid;tojid;msg;msgsize;pldsize";
            // String logPath = "logs/" + mTestName + "/receiver/r_" + mClientId + ".csv";

            mLogger = new MqttACDSLogger(getLogger());

            mLogger.setContext(mMqttClient);

            try {
                mLogger.onConnecting(mMqttClient.getClientId());
                mMqttClient.connect(mMqttOptions);
                mLogger.onConnected(mMqttClient.getClientId());
                mLogger.onSubscribe(mTopic);
                mMqttClient.subscribe(mTopic);
            } catch (MqttException e) {
                mLogger.onError(e);
            }

            // mRecvController.receiverBuiltUpdate(this);

        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void shutdown() {
        if (mMqttClient != null && mMqttClient.isConnected())
            try {
                mLogger.onDisconnecting(mMqttClient.getClientId());
                mMqttClient.disconnect();
                mLogger.onDisconnected(mMqttClient.getClientId());
            } catch (MqttException e) {
                e.printStackTrace();
            }

        LOGGER.info("{} >> shut down connection", mMqttClient.getClientId());
        super.shutdown();
    }

    // /**
    // *
    // * @param senderHost
    // * @param receiverHost
    // * @param testName
    // */
    // public void testDiscoveryTime(String senderHost, String receiverHost) {
    // // TODO header sieht komisch aus, sleep-Zeit fließt ein etc
    // // String header = "id;time;fromjid;tojid;msg;msgsize;pldsize";
    // ZeebusLogger discoLogger = getDiscoveryLogger();
    //
    // long timeBeforeDisco = System.currentTimeMillis();
    // List<String> results = discoverTopics(senderHost, receiverHost);
    // long discoLatency = System.currentTimeMillis() - timeBeforeDisco;
    //
    // discoLogger.logDiscovery(
    // // erstmal wie bei MUC, mal fragen wie header oben gemeint war
    // results.size(), timeBeforeDisco, 0, timeBeforeDisco + discoLatency, 0, 0, discoLatency);
    //
    // discoLogger.shutDown();
    // }

    // /**
    // *
    // * @param senderHost
    // * @param receiverHost
    // * @param testName
    // * @return
    // */
    // public List<String> discoverTopics(String senderHost, String receiverHost) {
    // MqttDiscovery discovery = new MqttDiscovery(senderHost);
    // String discoTopic = getTestName() + "/discovery";
    //
    // final List<String> fTopics = new ArrayList<String>();
    // // final String fTestName = testName;
    //
    // MqttDiscovery.setDiscoveryListener(new DiscoveryListener() {
    // @Override
    // public void onDiscover(DiscoveryTable discoveryTable, String topic) {
    // if (topic.contains(getTestName()))
    // fTopics.add(topic);
    // }
    // });
    //
    // discovery.discoverTopic(discoTopic);
    //
    // // TODO ist Mist, über Listener die Liste zu füllen und nach fester Zeit zurückzugeben.
    // // Tristan hatte in onDiscover() jedes Mal gleich einen Receiver erstellt, sollte aber irgendwie einheitlich
    // // sein
    // try {
    // Thread.sleep(5000);
    // } catch (InterruptedException e) {
    // LOGGER.error("[ERROR] Failed to sleep while waiting for topic discoveries");
    // }
    //
    // // contains static stuff
    // discovery.clearDiscoveryTables();
    // discovery.shutDown();
    //
    // return fTopics;
    // }

    @Override
    public String toString() {
        return mMqttClient.toString();
    }
}
