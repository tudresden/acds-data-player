package de.tudresden.inf.rn.zeebus.emulation.acds;

import java.util.List;
import java.util.Map.Entry;

import de.tudresden.inf.rn.zeebus.emulation.client.AbstractClientEmulator;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;

public abstract class AbstractAcdsClientEmulator extends AbstractClientEmulator<ACDSSensorEvent> {
    
    private final EventQueue<ACDSSensorEvent> eventQueue;
    
    protected AbstractAcdsClientEmulator(String identifier, String testName, EventQueue<ACDSSensorEvent> eventQueue) {
        super(identifier, testName, eventQueue);
        this.eventQueue = eventQueue;
    }

    @Override
    public boolean isQueueEmpty() {
        return eventQueue.isEmpty();
    }

    @Override
    public EventQueue<ACDSSensorEvent> getEventQueue() {
        return eventQueue;
    }

    @Override
    public abstract void connect();

    @Override
    protected Entry<Long, List<ACDSSensorEvent>> getEarliestEvents() {
        return this.eventQueue.firstEntry();
    }

    @Override
    protected List<ACDSSensorEvent> popCurrentEvents() {
        long currentEventTimestamp = eventQueue.firstKey();
        return eventQueue.remove(currentEventTimestamp);
    }

    @Override
    protected abstract void sendMessage(ACDSSensorEvent event);

    @Override
    protected List<ACDSSensorEvent> removeEvents(long timestamp) {
        return this.eventQueue.remove(timestamp);
    }

}
