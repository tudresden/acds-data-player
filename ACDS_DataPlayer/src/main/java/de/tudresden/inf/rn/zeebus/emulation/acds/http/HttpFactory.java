package de.tudresden.inf.rn.zeebus.emulation.acds.http;

import java.io.File;
import java.util.List;

import de.tudresden.inf.rn.zeebus.emulation.acds.ACDSSensorEvent;
import de.tudresden.inf.rn.zeebus.emulation.acds.AbstractAcdsEmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.Logging;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;

public class HttpFactory extends AbstractAcdsEmulatorFactory<HttpClientEmulator> {


    public HttpFactory(String testName, File[] scripts, String senderHost, String userHost, int userPerClient) {
        super(testName, scripts, senderHost, userHost, userPerClient);
    }

    @Override
    protected HttpClientEmulator buildEmulator(EventQueue<ACDSSensorEvent> eventQueue, String[] clientDescription) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void configureLogging(HttpClientEmulator clientEmulator, Logging logging) {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected void testDiscoveryTime() {
        // TODO Auto-generated method stub
    }

    @Override
    protected List<String> discoverTopics() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected UserEmulator internalCreateUser(String topic, int i, Logging logging) {
        // TODO Auto-generated method stub
        return null;
    }

}
