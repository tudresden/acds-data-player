package de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.muc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.muc.MultiUserChat;

import de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.AbstractXmppUserEmulator;

public class XmppMucUserEmulator extends AbstractXmppUserEmulator {
    private static final Logger LOGGER = LogManager.getLogger(AbstractXmppUserEmulator.class);
    private String mMucJid;
    private MultiUserChat mMuc;

    public XmppMucUserEmulator(String testName, String host, String xmppAccount, String xmppPassword, String xmppResource,
                           String mucJid) {
        super(testName, host, xmppAccount, xmppPassword, xmppResource);

        mMucJid = mucJid;
        mRunMode = Mode.INIT;
    }

    @Override
    protected void init() {
        connect();
        login();

        try {
            mMuc = new MultiUserChat(mConnection, mMucJid);
            mMuc.join(mXmppResource);

        } catch (XMPPException | NoResponseException | NotConnectedException e) {
            LOGGER.error(mXmppAccount + "/" + mXmppResource + ": receiver could not join muc " + mMuc.getRoom() + " ("
                    + e.getMessage() + ")");
            return;
        }

        mMuc.addMessageListener(new PacketListener() {

            @Override
            public void processPacket(Packet packet) {

                if (getLogger() != null) {
                    Message message = (Message)packet;
                    int payloadByteSize = 0;
                    if (message.getBody() != null) {
                        payloadByteSize = message.getBody().getBytes().length;
                    }

                    // TODO @klemens logger will not be closed appropriately
                    getLogger().logState(
                            message.getPacketID(), System.currentTimeMillis(), message.getFrom(), message.getTo(), "p",
                            payloadByteSize, message.toXML().toString().getBytes().length);
                }

                mMuc.pollMessage();
            }
        });
    }

    @Override
    public void shutdown() {
        disconnect();
        super.shutdown();
    }
}
