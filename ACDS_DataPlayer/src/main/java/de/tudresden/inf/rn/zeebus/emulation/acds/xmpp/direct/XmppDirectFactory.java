package de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.direct;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import de.tudresden.inf.rn.zeebus.emulation.acds.ACDSSensorEvent;
import de.tudresden.inf.rn.zeebus.emulation.acds.xmpp.AbstractXmppEmulatorFactory;
import de.tudresden.inf.rn.zeebus.emulation.client.Logging;
import de.tudresden.inf.rn.zeebus.emulation.events.EventQueue;
import de.tudresden.inf.rn.zeebus.emulation.user.UserEmulator;

public class XmppDirectFactory extends AbstractXmppEmulatorFactory<XmppDirectClientEmulator> {


    public XmppDirectFactory(String testName, Collection<File> scripts, String senderHost, String userHost,
            int userPerClient) {
        super(testName, scripts, senderHost, userHost, userPerClient);
    }
    
    public XmppDirectFactory(String testName, File[] loadScripts, String senderHost, String userHost, int userPerSender) {
        this(testName, Arrays.asList(loadScripts), senderHost, userHost, userPerSender);
    }

    @Override
    protected XmppDirectClientEmulator buildEmulator(EventQueue<ACDSSensorEvent> eventQueue, String[] clientDescription) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void testDiscoveryTime() {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected List<String> discoverTopics() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected UserEmulator internalCreateUser(String topic, int i, Logging logging) {
        // TODO Auto-generated method stub
        return null;
    }

}
