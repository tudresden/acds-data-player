/*
 * Created at 10:23:10 on 14.08.2014
 */
package de.tudresden.inf.rn.zeebus.performance;

/**
 * <p>
 * Interface to mark performance tests. So they do not run upon {@code mvn install}.
 * </p>
 * 
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
public interface PerformanceTest {

}
