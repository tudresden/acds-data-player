/*
 * Created at 08:21:23 on 13.08.2014
 */
package de.tudresden.inf.rn.zeebus.performance;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.tudresden.inf.rn.zeebus.DataPlayer;
import de.tudresden.inf.rn.zeebus.DataPlayerListener;
import de.tudresden.inf.rn.zeebus.emulation.acds.AcdsDataPlayer;

/**
 * @author Klemens Muthmann
 *
 * @version 1.0
 * @since 1.0
 */
@Category(PerformanceTest.class)
public class PerformanceTests {

    @Test
    public void testXMPPMUCRun() throws InterruptedException, IOException {
        runTest("/config-xmppmuc.cfg");
    }

    @Test
    public void testXMPPPUBSUBRun() throws InterruptedException, IOException {
        runTest("/config-xmpppubsub.cfg");
    }

    @Test
    public void testMQTTRun() throws InterruptedException, IOException {
        runTest("/config-mqtt.cfg");
    }

    private void runTest(final String configResource) throws InterruptedException, IOException {
        InputStream configFileLocation = PerformanceTests.class.getResourceAsStream(configResource);

        try {
            final ReentrantLock lock = new ReentrantLock();
            final Condition condition = lock.newCondition();

            DataPlayer player = new AcdsDataPlayer();
            player.run(configFileLocation, new DataPlayerListener() {
                public void playerFinished() {
                    lock.lock();
                    try {
                        condition.signal();
                    } finally {
                        lock.unlock();
                    }
                }
            });

            lock.lock();
            try {
                condition.await();
            } finally {
                lock.unlock();
            }
        } finally {
            configFileLocation.close();
        }
    }

}
