package de.tudresden.inf.rn.Parser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import de.tudresden.inf.rn.Parser.filter.FilenameExtensionFilter;

public class MakeRDFValidHelper {

	
	Boolean xmlHeadRead = false;
	Boolean deleteMode = false;
	FileReader input = null;
	FileWriter output = null;
	
	private File systems[];
	
	public MakeRDFValidHelper(String path_temp){
		System.out.println("Make valid RDF File...(2/3)");
		File dirSens = new File(path_temp);
		systems = dirSens.listFiles(new FilenameExtensionFilter(".rdf"));
		makeValid();
		System.out.println("Done!");
	}
		
	public void makeValid(){
		
		for(int i=0;i<systems.length;i++){
			try {
				input = new FileReader(systems[i]);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				output = new FileWriter(systems[i]+"_");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try{
				BufferedReader input2 = new BufferedReader(input);
				BufferedWriter output2 = new BufferedWriter(output);
				String zeile = null;
				while ((zeile = input2.readLine()) != null) {
					
					//Build XML Head
					if((zeile.equals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>") && !xmlHeadRead)){
							xmlHeadRead = true;
							output2.write(zeile+"\n");
							output2.append("<rdf:RDF\n"
									+ "\txmlns:om-owl=\"http://knoesis.wright.edu/ssw/ont/sensor-observation.owl#\"\n"
									+ "\txmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"\n"
									+ "\txmlns:sens-obs=\"http://knoesis.wright.edu/ssw/\"\n"
									+ "\txmlns:owl-time=\"http://www.w3.org/2006/time#\"\n"
									+ "\txmlns:owl=\"http://www.w3.org/2002/07/owl#\"\n"
									+ "\txmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"\n"
									+ "\txmlns:weather=\"http://knoesis.wright.edu/ssw/ont/weather.owl#\"\n"
									+ "\txmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
									+ "\txmlns:wgs84=\"http://www.w3.org/2003/01/geo/wgs84_pos#\">\n");
					}
	
					//Delete following Heads appended in Doc
					else if(zeile.equals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>") 
							|| (zeile.equals("</rdf:RDF><?xml version=\"1.0\" encoding=\"UTF-8\"?>"))
							|| (zeile.equals("<rdf:RDF"))){
							deleteMode = true;
					}
					
					else if(deleteMode && zeile.contains("#\">")){
							deleteMode = false;
					}
					
					//Adds regular lines
					else if (!deleteMode){
						output2.append(zeile+"\n");
					}
					//System.out.println("Gelesene Zeile: " + zeile);
				}
				
				output2.close();
			
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				input.close();
				xmlHeadRead = false;
				deleteMode = false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//delete old temp file
			System.out.println("Deleting file " + systems[i] + ": " +systems[i].delete());
		}
			
	}

	
	
	
}
