package de.tudresden.inf.rn.Parser.filter;

import java.io.File;
import java.io.FilenameFilter;

public class FilenameExtensionFilter implements FilenameFilter {
	
	String str;
	
	public FilenameExtensionFilter(String ex)
	   {
	      str=ex;
	   }
	
	@Override
	public boolean accept(File dir, String name) {
	     return (name.endsWith(str));
	}

}
