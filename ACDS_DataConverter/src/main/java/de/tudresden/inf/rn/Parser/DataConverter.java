package de.tudresden.inf.rn.Parser;

public class DataConverter {

	//standard is data/sens and data/obs
	private static String path_sens = "data/sens";
	private static String path_obs = "data/obs";
	
	//standard is data/temp
	private static String path_temp = "data/temp";
	
	//standard is data/scripts
	private static String path_scripts = "data/scripts";
	
	
	public static void main(String[] args) {
		new N3ToRDF(path_sens, path_obs, path_temp);
		new MakeRDFValidHelper(path_temp);
		new RDFParser(path_temp, path_scripts);
	}

}
