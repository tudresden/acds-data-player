package de.tudresden.inf.rn.Parser.Enum;

public class MeasureDataTypes {
	public static final int 
	time=0,
	DewPoint=1,	
	RelativeHumidity=2,
	WindDirection=3,
	Pressure=4,
	AirTemperature=5,
	Precipitation=6,
	WindSpeed=7,
	WindGust=8;

	
	public static int getType(String type){
		if(type.equals("time"))
			return time;
		if(type.equals("DewPoint"))
			return DewPoint;
		if(type.equals("RelativeHumidity"))
			return RelativeHumidity;
		if(type.equals("WindDirection"))
			return WindDirection;
		if(type.equals("Pressure"))
			return Pressure;
		if(type.equals("AirTemperature"))
			return AirTemperature;
		if(type.equals("Precipitation"))
			return Precipitation;
		if(type.equals("WindSpeed"))
			return WindSpeed;
		if(type.equals("WindGust"))
			return WindGust;
		return -1;
	}
}
