package de.tudresden.inf.rn.Parser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



import org.xml.sax.SAXParseException;

import de.tudresden.inf.rn.Parser.Enum.MeasureDataTypes;
import de.tudresden.inf.rn.Parser.filter.FilenameExtensionFilter;



public class RDFParser {
		
	Document document;
	
	FileOutputStream fop = null;
	FileOutputStream fop2 = null;
	private File systems[];
	private File parsedFile;
	private String processingFilePath ="";
	
	private Map<String, Integer> headerMap;
	//positions in csv
	//int time=0, dewpoint=1, relhum=2, winddir=3, pres=4, airtemp=5, prec=6, windspd=7, windgust=8;
	
	//key is timestamp id of a found obs-event, List contains Data: timestamp, sensor, w_data_1, w_data_2...
	private Map<String, String[]> csv;
	
	//meta data of the system
	private Map<String, String> metaData;
	
	private Map<String, String> dataUnits;
	
	private String path_temp;
	private String path_scripts;
	
	public RDFParser(String path_temp, String path_scripts){
		this.path_temp = path_temp;
		this.path_scripts = path_scripts;
		
		System.out.println("Converting from RDF to SensorEvent CSV... (3/3)");
		File dirSens = new File(this.path_temp);
		systems = dirSens.listFiles(new FilenameExtensionFilter(".rdf_"));
		
		
		//setup document and paths
		start();
		
		System.out.println("Done!");
	}
	

	private void start() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			for(int i=0;i<systems.length;i++){
				parsedFile = systems[i];
				System.out.println("processing: " + parsedFile.toString());
				
				csv =  new HashMap<String, String[]>();
				headerMap = new HashMap<String, Integer>();
				String[] segs = parsedFile.getName().split( Pattern.quote( "." ) );
				processingFilePath = (path_scripts + "/" + segs[0] + ".csv");
				try{
				document = builder.parse( parsedFile );
				//get Measured Data Types and generate Header
				getHeader();
				//get Timestamps & System Informations
				read();
				//collect Measured Data for Timestamps
				getDataForIDs();
				writeToCSV();
				
				//System.out.println("in " + (date2.getTime()-date1.getTime()) + "ms");
				}
				catch(SAXParseException e){
					System.out.println("Fehler beim Parsen der Datei: " + parsedFile.toString() + ". Grund: " + e.toString());
				}
			
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//generate Measured Data Header for csv
	private void getHeader() {
		Set<String> headerSet = new TreeSet<String>();
		NodeList nodeList = document.getFirstChild().getChildNodes();
		
		for(int f=0;f<nodeList.getLength();f++){
			Node node = nodeList.item(f);
			
			//attributes of first childs
			if((nodeList.item(f)).hasAttributes()){
				NamedNodeMap nodeMap = (nodeList.item(f)).getAttributes();
				for (int h = 0; h < nodeMap.getLength(); h++) {
					Node node2 = nodeMap.item(h);
					if(node2.getNodeValue().contains("System_")){
						NodeList nodeList2 = node.getChildNodes();
						
						for(int j=0;j<nodeList2.getLength();j++){
							if(nodeList2.item(j).getNodeName().contains("generatedObservation")){
								//h=nodeMap.getLength();
								//f=nodeList.getLength();
								
								if(nodeList2.item(j).hasAttributes()){
									NamedNodeMap nodeMap2 = nodeList2.item(j).getAttributes();
									for(int k=0;k<nodeMap2.getLength();k++){
										Node node3 = nodeMap2.item(k);
										if(node3.getNodeValue().contains("http://knoesis.wright.edu/ssw/Observation_")){
											String dataType = node3.getNodeValue();
											dataType = dataType.replace("http://knoesis.wright.edu/ssw/Observation_", "");
											String[] dataType2 = dataType.split("_", 2);
											//System.out.println(dataType2[0]);
											headerSet.add(dataType2[0]);
										}
									}
								}
								
								
							}
						}
					}
				}
			}
		}
		
		//cell 0 is reserved for timestamp Id and timestamp
		int cell=1;
		for(String dataString : headerSet){
			headerMap.put(dataString, cell);
			cell++;
		}
		
	}

	public void read(){
		metaData = new HashMap<String, String>();
		
		NodeList nodeList = document.getFirstChild().getChildNodes();
		
		//Instant child elements of root
		for(int f=0;f<nodeList.getLength();f++){
			Node node = nodeList.item(f);
			String[] eventData = new String[1+headerMap.size()];
			String key;
			
			//attributes of first childs
			if((nodeList.item(f)).hasAttributes()){
				NamedNodeMap nodeMap = (nodeList.item(f)).getAttributes();
				for (int h = 0; h < nodeMap.getLength(); h++) {
					Node node2 = nodeMap.item(h);
					
					//Get Timestamps
					if(node2.getNodeValue().contains("Instant_")){
						key = node2.getNodeValue();
		    			key = key.replace("http://knoesis.wright.edu/ssw/Instant_","");
		    			//System.out.println(key);
		    			NodeList nodeList2 = node.getChildNodes();
		    			
		    			
		    			//child of Instant Node
		    			for(int j=0;j<nodeList2.getLength();j++){
		    				//System.out.println((nodeList2.item(j)).getTextContent());
		    				
		    				//exctract time
		    				NodeList nodeList3 = nodeList2.item(j).getChildNodes();
		    				for(int i2=0;i2<nodeList3.getLength();i2++){
			    				String event = nodeList3.item(i2).getTextContent();
								event = event.replace("^^http://www.w3.org/2001/XMLSchema#dateTime", "");
			    				eventData[0]=event;
		    				}
		    			}
		    			
		    			csv.put(key, eventData);
					}
					
					//system location
					if(node2.getNodeValue().contains("http://knoesis.wright.edu/ssw/point_")){
						NodeList nodeList2 = node.getChildNodes();
		    			
		    			
		    			//childs of point Node
		    			for(int j=0;j<nodeList2.getLength();j++){
		    				if(nodeList2.item(j).getNodeName().equals("wgs84:alt")){
		    					//System.out.println(nodeList2.item(j).getTextContent());
		    					metaData.put("alt", nodeList2.item(j).getTextContent());
		    				}
		    				if(nodeList2.item(j).getNodeName().equals("wgs84:lat")){
		    					//System.out.println(nodeList2.item(j).getTextContent());
		    					metaData.put("lat", nodeList2.item(j).getTextContent());
		    				}
		    				if(nodeList2.item(j).getNodeName().equals("wgs84:long")){
		    					//System.out.println(nodeList2.item(j).getTextContent());
		    					metaData.put("long", nodeList2.item(j).getTextContent());
		    				}
		    			}
					}
					
					
					//general system info 1
					if(node2.getNodeValue().contains("http://knoesis.wright.edu/ssw/System_")){
						NodeList nodeList2 = node.getChildNodes();
		    			
		    			
		    			//childs of point Node
		    			for(int j=0;j<nodeList2.getLength();j++){
		    				if(nodeList2.item(j).getNodeName().equals("om-owl:hasSourceURI")){
		    					if(nodeList2.item(j).hasAttributes()){
		    						//System.out.println(((nodeList2.item(j).getAttributes()).item(0)).getNodeValue());
		    						metaData.put("url", ((nodeList2.item(j).getAttributes()).item(0)).getNodeValue());
		    					}
		    				}
		    				if(nodeList2.item(j).getNodeName().equals("om-owl:ID")){
		    					//System.out.println(nodeList2.item(j).getTextContent());
		    					metaData.put("id", nodeList2.item(j).getTextContent());
		    				}
		    			}
					}
					
					if(node2.getNodeValue().contains("http://knoesis.wright.edu/ssw/LocatedNearRel")){
						NodeList nodeList2 = node.getChildNodes();
		    			
		    			
		    			//childs of point Node
		    			for(int j=0;j<nodeList2.getLength();j++){
		    				if(nodeList2.item(j).getNodeName().equals("om-owl:hasLocation")){
		    					if(nodeList2.item(j).hasAttributes()){
		    						//System.out.println(((nodeList2.item(j).getAttributes()).item(0)).getNodeValue());
		    						metaData.put("locationName", ((nodeList2.item(j).getAttributes()).item(0)).getNodeValue());
		    						
		    					}
		    				}
		    				if(nodeList2.item(j).getNodeName().equals("om-owl:distance")){
		    					//System.out.println(nodeList2.item(j).getTextContent());
		    					metaData.put("locationDistance", nodeList2.item(j).getTextContent());
		    				}
		    			}
					}
					
				}
			}
		}
		
	}
	
	private void writeToCSV(){
		//just write if meta data is complete
		if(checkMetaData()){
			try {
				fop = new FileOutputStream(processingFilePath);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			//write System meta information
			Set<String> keys = metaData.keySet();
			String systemInfoString="";
			//String dataValueString="";
			String separator=";";
			int counter=0;
			for(String key : keys){
				if(counter==6) separator="";
				systemInfoString = systemInfoString+ key + "=" + metaData.get(key) + separator;
				//dataValueString = dataValueString + metaData.get(key) + separator;
				counter++;
			}
			try {
				byte[] contentInBytes = (systemInfoString+"\n").getBytes();
				fop.write(contentInBytes);
				//contentInBytes = (dataValueString+"\n").getBytes();
				//fop.write(contentInBytes);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			//write info header
			String[] headerHelper = new String[1+headerMap.size()];
			headerHelper[0] = "Timestamp";
			for(String key : headerMap.keySet()){
				headerHelper[headerMap.get(key)] = key;
			}
			
			String header1String="";
			for(int i0=0;i0<headerHelper.length;i0++){
				String unit;
				if(i0<headerHelper.length-1){
					unit = dataUnits.get(headerHelper[i0]);
					header1String = header1String + headerHelper[i0] + ":" + unit + ";";
				} 
				else {
					unit = dataUnits.get(headerHelper[i0]);
					header1String = header1String + headerHelper[i0] + ":" + unit;
				}
			}
			
			try {
				byte[] contentInBytes = (header1String+"\n").getBytes();
				fop.write(contentInBytes);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			//write content
			Collection<String[]> content = sortCSV().values();
			Iterator<String[]> iter1 = content.iterator();
			
			while(iter1.hasNext()){
				String[] lineAsList = iter1.next();
				String line="";
				for(int i=0; i<lineAsList.length;i++){
					if(lineAsList[i]==null){
						lineAsList[i]="";
					}
					if(i==lineAsList.length-1){
						line = line + lineAsList[i];
					}
					else{
						line = line + lineAsList[i] + ";";
					}
				}
				
				try {
					byte[] contentInBytes = (line+"\n").getBytes();
					fop.write(contentInBytes);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
	}
	
	private TreeMap<String, String[]> sortCSV() {
		TreeMap<String, String[]> sortedCSV = new TreeMap<String,String[]>();
		for(String[] csvLine : csv.values()){
			sortedCSV.put(csvLine[0], csvLine);
		}
		return sortedCSV;
	}


	private boolean checkMetaData() {
		if(!(metaData.keySet().size()!=7)){
			for(String key : metaData.keySet()){
				if(metaData.get(key)==null) return false;
			}
			return true;
		}
		else{
			return false;
		}
	}


	private void getDataForIDs(){
		dataUnits = new HashMap<String, String>();
		dataUnits.put("Timestamp", "UTC");
		NodeList nodeList = document.getFirstChild().getChildNodes();
		for(int i1=0; i1<nodeList.getLength();i1++){
			String dataInfo=null;
			
			//description element below root
			Node element = nodeList.item(i1);
			if(element.hasAttributes()){
				NamedNodeMap nodeMap = element.getAttributes();
				for(int i2=0;i2<nodeMap.getLength();i2++){
					dataInfo = nodeMap.item(i2).getNodeValue();
				}
			}
			if(dataInfo!=null){
				if(dataInfo.contains("http://knoesis.wright.edu/ssw/MeasureData_")){
					dataInfo = dataInfo.replace("http://knoesis.wright.edu/ssw/MeasureData_","");
					String[] data = dataInfo.split("_", 3);
					
					if(csv.containsKey(data[2])){
						String[] stringArray = csv.get(data[2]);
						
						NodeList nl = element.getChildNodes();
						for(int i3=0;i3<nl.getLength();i3++){
							if(nl.item(i3).hasChildNodes()){
								//discard not generally supported MeasureData
								if(MeasureDataTypes.getType(data[0])>0){
									try{
									stringArray[headerMap.get(data[0])] = nl.item(i3).getFirstChild().getTextContent();
									}
									catch(NullPointerException e){
										System.out.println(e + data[0] + data[1] + data[2] + headerMap.get(data[0]) + headerMap.size());
									}
								}
							}
						}
					}
					
					if(dataUnits.get(data[0])==null){
						NodeList nl = element.getChildNodes();
						
						for(int i3=0;i3<nl.getLength();i3++){
							if(nl.item(i3).hasChildNodes()){
								if((nl.item(i3).getNextSibling().getNextSibling().getAttributes().item(0))
										.getNodeValue().contains("http://knoesis.wright.edu/ssw/ont/weather.owl#")){
									String unit = (nl.item(i3).getNextSibling().getNextSibling().getAttributes().item(0))
											.getNodeValue().replace("http://knoesis.wright.edu/ssw/ont/weather.owl#", "");
									dataUnits.put(data[0], unit);
								}
							}
						}
					}
				}
			}
		}
	}
}
