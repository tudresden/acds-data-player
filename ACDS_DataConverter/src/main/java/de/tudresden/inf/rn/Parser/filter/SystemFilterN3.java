package de.tudresden.inf.rn.Parser.filter;

import java.io.File;
import java.io.FilenameFilter;

public class SystemFilterN3 implements FilenameFilter {
	
	String str;
	
	public SystemFilterN3(String ex)
	   {
	      str=ex;
	   }
	
	@Override
	public boolean accept(File dir, String name) {
	     return (name.startsWith(str)&&name.endsWith(".n3"));
	}

}
