package de.tudresden.inf.rn.Parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;



import de.l3s.sesame2.tools.RDF2RDF;
import de.tudresden.inf.rn.Parser.filter.SystemFilterN3;

public class N3ToRDF {
	
	private File systems[];
	private String systemName="";
	private String path_sens;
	private String path_obs;
	private String path_temp;
	
	public N3ToRDF(String path_sens, String path_obs, String path_temp){
		this.path_obs = path_obs;
		this.path_sens = path_sens;
		this.path_temp = path_temp;
		convert();
	}
	
	public void convert(){
		System.out.println("Converting from N3 to RDF (1/3)");
		getAllSystems();
		System.out.println("Done!");
		//getAllObservationsForSystem("");
		//convertSystemToRDF();
	}
	
	//get all Sensor Systems
	private void getAllSystems(){
		File dirSens = new File(path_sens);
		systems = dirSens.listFiles();
		for(int i=0;i<systems.length;i++){
			String filename = systems[i].getName();
			String[] segs = filename.split( Pattern.quote( "_" ) );
			//System.out.println("System:" + systems[i].getName());
			systemName = segs[0];
			getAllObservationsForSystem(systems[i], segs[0]+"_");
		}
	}
	
	private void getAllObservationsForSystem(File sensFile, String filenamePrefixSystem){
		File dirObs = new File(path_obs);
		List<String> paths = new ArrayList<String>();
		paths.add(sensFile.toString());
		//String[] paths = {sensFile.toString()};
		//FilenameFilter filterN3 = new SystemFilter("A21");
		for ( File file : dirObs.listFiles(new SystemFilterN3(filenamePrefixSystem)) ){
			paths.add(file.toString());
			//System.out.println( file );
		}
		convertSystemToRDF(paths);
	}
	
	private void convertSystemToRDF(List<String> paths){
		String[] args = paths.toArray(new String[paths.size()+1]);
		args[args.length-1] = path_temp + "/"+ systemName +".rdf"; 
		try {
			RDF2RDF.main(args);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


